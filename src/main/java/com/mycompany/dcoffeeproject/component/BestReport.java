/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.dcoffeeproject.component;

import com.mycompany.dcoffeeproject.model.PosReport;
import com.mycompany.dcoffeeproject.service.ReportService;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author kanthapong
 */
public class BestReport extends javax.swing.JPanel {

    private final ReportService reportService;
    private AbstractTableModel model;
     private final List<PosReport> reportList;
    private DefaultCategoryDataset barDataset;
    private DefaultPieDataset pieDataset;

    /**
     * Creates new form BestReport
     */
    public BestReport() {
        initComponents();
         reportService = new ReportService();
         reportList = reportService.getTopFiveProductByTotalQuantity();
         initTable1();
         initPieChart();
         loadPieDataset();
         
    }

    
    private void initTable1() {
        model = new AbstractTableModel() {
            String[] colName = {"ID", "Name", "Quantity"};

            @Override
            public String getColumnName(int column) {
                return colName[column];
            }

            @Override
            public int getRowCount() {
                return reportList.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                PosReport report = reportList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getId();
                    case 1:
                        return report.getName();
                    case 2:
                        return report.getQty();
                    default:
                        return "";
                }
            }
        };
        tblBest.setModel(model);
    }
    
//    private void initBarChart() {
//        barDataset = new DefaultCategoryDataset();
//        JFreeChart chart = ChartFactory.createBarChart(
//                "Sales",
//                "Month",
//                "Profit",
//                barDataset,
//                PlotOrientation.VERTICAL,
//                true, true, false);
//        ChartPanel chartPanel = new ChartPanel(chart);
//        pnlBarGraph.add(chartPanel);
//    }
//
//    private void loadBarDataset() {
//        barDataset.clear();
//        for (PosReport a : reportList) {
//            barDataset.setValue(a.getQty(), "Values", a.getTotalPrice());
//        }
//    }
    
    private void initPieChart() {
        pieDataset = new DefaultPieDataset();
        JFreeChart chart = ChartFactory.createPieChart("Best Seller", // chart title
                pieDataset, // data
                true, // include legend
                true,
                false);
        ChartPanel chartPanel = new ChartPanel(chart);
        pnlPieGraph.add(chartPanel);
    }

    private void loadPieDataset() {
        pieDataset.clear();
        for (PosReport a : reportList) {
            pieDataset.setValue(a.getName(), a.getQty());
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblBest = new javax.swing.JTable();
        pnlPieGraph = new javax.swing.JPanel();

        tblBest.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblBest);

        pnlPieGraph.setBackground(new java.awt.Color(48, 8, 40));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(pnlPieGraph, javax.swing.GroupLayout.PREFERRED_SIZE, 995, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlPieGraph, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 435, Short.MAX_VALUE))
                .addGap(50, 50, 50))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 446, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pnlPieGraph;
    private javax.swing.JTable tblBest;
    // End of variables declaration//GEN-END:variables
}
