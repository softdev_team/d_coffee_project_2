/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.dcoffeeproject.component;

import com.mycompany.dcoffeeproject.model.Product;

/**
 *
 * @author chanatip
 */
public interface BuyProductable {

    public void buy(Product product,  String productType, String ProductSize, int qty);
}
