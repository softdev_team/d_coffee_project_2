/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kewwy
 */
public class User {
    private int id ;
    private String name ;
    private String login;
    private String password ;
    private int role ;
    private String tel ;
    private int hourWage;

    public User(int id, String name, String login, String password, int role , String tel, int hourWage) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.password = password;
        this.role = role;
        this.tel = tel;
        this.hourWage = hourWage;
    }
    
    public User(String name, String login, String password, int role, String tel,int hourWage) {
        this.id = -1;
        this.name = name;
        this.login = login;
        this.password = password;
        this.role = role;
        this.tel = tel;
        this.hourWage = hourWage;
        
    }
    
     public User() {
        this.id = -1;
        this.name = "";
        this.login = "";
        this.password = "";
        this.role = 0;
        this.tel = "";
        this.hourWage = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getHourWage() {
        return hourWage;
    }

    public void setHourWage(int hourWage) {
        this.hourWage = hourWage;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", login=" + login + ", password=" + password + ", role=" + role + ", tel=" + tel + ",hourWage=" + hourWage + '}';
    }
     
    

   

    public static User fromRS(ResultSet rs) {
        User user = new User();
        try {
            user.setId(rs.getInt("user_id"));
            user.setName(rs.getString("user_name"));
            user.setLogin(rs.getString("user_login"));
            user.setPassword(rs.getString("user_password"));
            user.setRole(rs.getInt("user_role"));
            user.setTel(rs.getString("user_tel"));
            user.setHourWage(rs.getInt("user_hourWage"));
            
            
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return user;
    }

}
