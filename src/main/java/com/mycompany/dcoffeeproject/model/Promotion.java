/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chanatip
 */
public class Promotion {
    private int id;
    private String name;
    private float discount;
    private int requiredPoints;
    private int earnedPoints;

    public Promotion(int id, String name, float discount, int requiredPoints, int earnedPoints) {
        this.id = id;
        this.name = name;
        this.discount = discount;
        this.requiredPoints = requiredPoints;
        this.earnedPoints = earnedPoints;
    }
    
    public Promotion(String name, float discount, int requiredPoints, int earnedPoints) {
        this.id = -1;
        this.name = name;
        this.discount = discount;
        this.requiredPoints = requiredPoints;
        this.earnedPoints = earnedPoints;
    }
    
    public Promotion() {
        this.id = -1;
        this.name = "";
        this.discount = 0;
        this.requiredPoints = 0;
        this.earnedPoints = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public int getRequiredPoints() {
        return requiredPoints;
    }

    public void setRequiredPoints(int requiredPoints) {
        this.requiredPoints = requiredPoints;
    }

    public int getEarnedPoints() {
        return earnedPoints;
    }

    public void setEarnedPoints(int earnedPoints) {
        this.earnedPoints = earnedPoints;
    }

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", discount=" + discount + ", requiredPoints=" + requiredPoints + ", earnedPoints=" + earnedPoints + '}';
    }
    
    public static Promotion fromRS(ResultSet rs) {
    Promotion promotion = new Promotion();
    try {
        promotion.setId(rs.getInt("promotion_id"));
        promotion.setName(rs.getString("promotion_name"));
        promotion.setDiscount(rs.getFloat("promotion_discount"));
        promotion.setRequiredPoints(rs.getInt("promotion_required_points"));
        promotion.setEarnedPoints(rs.getInt("promotion_earned_points"));
    } catch (SQLException ex) {
        Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
        return null;
    }
    return promotion;
}

}
