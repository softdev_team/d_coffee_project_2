/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.UserDao;
import com.mycompany.dcoffeeproject.dao.VendorDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USER
 */
public class Bill {

    private int id;
    private Date createdDate;
    private float totalBill;
    private float buy;
    private float change;
    private int userId;
    private int vendorId;
    private User user;
    private Vendor vendor;
    private ArrayList<BillDetail> billDetails = new ArrayList<BillDetail>(); //can add bill detail in bill or เวลา save จะไปเพิ่ม BillDetail ด้วย

    public Bill(int id, Date createdDate, float totalBill, float buy, float change, int userId, int vendorId) {
        this.id = id;
        this.createdDate = createdDate;
        this.totalBill = totalBill;
        this.buy = buy;
        this.change = change;
        this.userId = userId;
        this.vendorId = vendorId;
    }

    public Bill(Date createdDate, float totalBill, float buy, float change, int userId, int vendorId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.totalBill = totalBill;
        this.buy = buy;
        this.change = change;
        this.userId = userId;
        this.vendorId = vendorId;
    }

    public Bill(float totalBill, float buy, float change, int userId, int vendorId) {
        this.id = -1;
        this.createdDate = null;
        this.totalBill = totalBill;
        this.buy = buy;
        this.change = change;
        this.userId = userId;
        this.vendorId = vendorId;
    }

    public Bill(float buy, float change, int userId, int vendorId) {
        this.id = -1;
        this.createdDate = null;
        this.totalBill = 0;
        this.buy = buy;
        this.change = change;
        this.userId = userId;
        this.vendorId = vendorId;
    }

    public Bill() {
        this.id = -1;
        this.createdDate = null;
        this.totalBill = 0;
        this.buy = 0;
        this.change = 0;
        this.userId = 0;
        this.vendorId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        if (id >= 0) {
            this.id = id;
        } else {
            // จัดการกับค่าที่ไม่ถูกต้องตามที่คุณต้องการ
        }
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotalBill() {
        return totalBill;
    }

    public void setTotalBill(float totalBill) {
        this.totalBill = totalBill;
    }

    public float getBuy() {
        return buy;
    }

    public void setBuy(float buy) {
        this.buy = buy;
    }

    public float getChange() {
        return change;
    }

    public void setChange(float change) {
        this.change = change;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        if (user != null) {
            this.userId = user.getId();
        }
    }

    public Vendor getVendor() {
        return vendor;
    }

    public String getVendorName() {
        return (vendor != null) ? vendor.getName() : "N/A";
    }

    public void setVendor(Vendor vendor) {
        if (vendor != null) {
            this.vendor = vendor;
            this.vendorId = vendor.getId();
        }
    }

    public ArrayList<BillDetail> getBillDetails() {
        return billDetails;
    }

    public void setBillDetails(ArrayList billDetails) {
        this.billDetails = billDetails;
    }

    public void addBillDetail(BillDetail billDetail) {
        billDetails.add(billDetail);
        calculateBillTotal();
    }

    public void addBillDetail(Material material, int qty) {
        BillDetail bd = new BillDetail(material.getId(), material.getId(), material.getName(), qty, material.getUnit(), material.getPricePerUnit(), qty * material.getPricePerUnit(), -1);
        billDetails.add(bd);
        calculateBillTotal();
    }

    public void delBillDetail(ArrayList<BillDetail> billDetail) {
        billDetails.remove(billDetail);
        calculateBillTotal();

    }

    public void calculateBillTotal() {
        float totalBill = 0.0f;
        for (BillDetail billDetail : billDetails) {
            totalBill += billDetail.getTotal();
        }
        this.totalBill = totalBill;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", createdDate=" + createdDate + ", totalBill=" + totalBill + ", buy=" + buy + ", change=" + change + ", userId=" + userId + ", vendorId=" + vendorId + ", user=" + user + ", vendor=" + vendor + ", billDetails=" + billDetails + '}';
    }

    public static Bill fromRS(ResultSet rs) {
        Bill bill = new Bill();
        try {
            bill.setId(rs.getInt("bill_id"));
            bill.setCreatedDate(rs.getTimestamp("bill_date"));
            bill.setTotalBill(rs.getFloat("bill_total"));
            bill.setBuy(rs.getFloat("bill_buy"));
            bill.setChange(rs.getFloat("bill_change"));
            bill.setUserId(rs.getInt("user_id"));
            bill.setVendorId(rs.getInt("vendor_id"));

//            Populate
            UserDao userDao = new UserDao();
            VendorDao vendorDao = new VendorDao();
            User user = userDao.get(bill.getUserId());
            Vendor vendor = vendorDao.get(bill.getVendorId());
            bill.setUser(user);
            bill.setVendor(vendor);

        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bill;
    }

    public Bill get(int rowIndex) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public int size() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
