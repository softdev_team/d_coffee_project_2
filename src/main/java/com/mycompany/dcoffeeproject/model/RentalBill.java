/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mookda
 */
public class RentalBill {

    private int id;
    private Date date;
    private int price;

    public RentalBill(int id, Date date, int price) {
        this.id = id;
        this.date = date;
        this.price = price;
    }

    public RentalBill(Date date, int price) {
        this.id = -1;
        this.date = date;
        this.price = price;
    }

    public RentalBill() {
        this.id = -1;
        this.date = null;
        this.price = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "RentalBill{" + "id=" + id + ", date=" + date + ", price=" + price + '}';
    }

    public static RentalBill fromRS(ResultSet rs) {
        RentalBill rentalBill = new RentalBill();
        try {
            rentalBill.setId(rs.getInt("rental_bill_id"));
            rentalBill.setDate(rs.getTimestamp("rental_date"));
            rentalBill.setPrice(rs.getInt("rental_price"));
       
        } catch (SQLException ex) {
            Logger.getLogger(RentalBill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return rentalBill;
    }
}
