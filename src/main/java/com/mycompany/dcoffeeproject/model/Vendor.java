/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mookda
 */
public class Vendor {
    private int id ;
    private String name ;
    private String address ;
    private String tel ;

    public Vendor(int id, String name, String address, String tel) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.tel = tel;
    }
    
    public Vendor(String name, String address, String tel) {
        this.id = -1;
        this.name = name;
        this.address = address;
        this.tel = tel;
    }
    
     public Vendor() {
        this.id = -1;
        this.name = "";
        this.address = "";
        this.tel = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Vendor{" + "id=" + id + ", name=" + name + ", address=" + address + ", tel=" + tel + '}';
    }

    public static Vendor fromRS(ResultSet rs) {
        Vendor vendor = new Vendor();
        try {
            vendor.setId(rs.getInt("vendor_id"));
            vendor.setName(rs.getString("vendor_name"));
            vendor.setAddress(rs.getString("vendor_address"));
            vendor.setTel(rs.getString("vendor_tel"));

        } catch (SQLException ex) {
            Logger.getLogger(Vendor.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return vendor;
    }
}
