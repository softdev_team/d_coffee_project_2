/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.CheckInOutDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Salary {

    private static int get;
    private int id;
    private Date date;
    private Float workHour;
    private Float totalSalary;
   
    private CheckInOut checkinout;

    public Salary(int id,  Date date, Float workHour, Float totalSalary) {
        this.id = id;
        this.date = date;
        this.workHour = workHour;
        this.totalSalary = totalSalary;

    }

    public Salary( Date date, Float workHour, Float totalSalary) {
        this.id = -1;
        this.date = date;
        this.workHour = workHour;
        this.totalSalary = totalSalary;
     
    }
     public Salary() {
        this.id = -1;
        this.date = null;
        this.workHour = 0.00f;
        this.totalSalary = 0.00f;
        
    }

    public static int getGet() {
        return get;
    }

    public static void setGet(int get) {
        Salary.get = get;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Float getWorkHour() {
        return workHour;
    }

    public void setWorkHour(Float workHour) {
        this.workHour = workHour;
    }

    public Float getTotalSalary() {
        return totalSalary;
    }

    public void setTotalSalary(Float totalSalary) {
        this.totalSalary = totalSalary;
    }

   

    public CheckInOut getCheckinout() {
        return checkinout;
    }

    public void setCheckinout(CheckInOut checkinout) {
        this.checkinout = checkinout;
    }

    @Override
    public String toString() {
        return "Salary{" + "id=" + id + ", date=" + date + ", workHour=" + workHour + ", totalSalary=" + totalSalary + ", checkinout=" + checkinout + '}';
    }

  

     public static Salary fromRS(ResultSet rs) {
        Salary salary = new Salary();
        try {
            salary.setId(rs.getInt("salary_id"));
            salary.setDate(rs.getDate("salary_date"));
            salary.setWorkHour(rs.getFloat("salary_work_hour"));
            salary.setTotalSalary(rs.getFloat("salary_totalSalary"));
          
            
//            CheckInOutDao checkinoutDao = new CheckInOutDao();
//            CheckInOut checkinout = checkinoutDao.get(salary.getCheckinout().getId());
//            salary.setCheckinout(checkinout);

        } catch (SQLException ex) {
            Logger.getLogger(Store.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salary;
    }
    
}
