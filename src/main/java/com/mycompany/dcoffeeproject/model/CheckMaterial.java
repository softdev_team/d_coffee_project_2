/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.service.CheckMaterialService;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kewwy
 */
public class CheckMaterial {

    private int checkMatId;
    private String checkMatDate;
    private String checkMatTime;
    private int userId;
    private User user;
    private ArrayList<CheckMaterialDetail> checkMaterialDetails = new ArrayList<>();
    CheckMaterialService checkMaterialService = new CheckMaterialService();

    public CheckMaterial(int checkMatId, String checkMatDate, String checkMatTime, int userId) {
        this.checkMatId = checkMatId;
        this.checkMatDate = checkMatDate;
        this.checkMatTime = checkMatTime;
        this.userId = userId;
    }

    public CheckMaterial(String checkMatDate, String checkMatTime, int userId) {
        this.checkMatId = -1;
        this.checkMatDate = checkMatDate;
        this.checkMatTime = checkMatTime;
        this.userId = userId;
    }

    public CheckMaterial() {
        this.checkMatId = -1;
        this.checkMatDate = "";
        this.checkMatTime = "";
        this.userId = 0;
    }

    public int getCheckMatId() {
        return checkMatId;
    }

    public void setCheckMatId(int checkMatId) {
        this.checkMatId = checkMatId;
    }

    public String getCheckMatDate() {
        return checkMatDate;
    }

    public void setCheckMatDate(String checkMatDate) {
        this.checkMatDate = checkMatDate;
    }

    public String getCheckMatTime() {
        return checkMatTime;
    }

    public void setCheckMatTime(String checkMatTime) {
        this.checkMatTime = checkMatTime;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        if (user != null) {
            this.userId = user.getId();
        }
    }

    public ArrayList<CheckMaterialDetail> getCheckMaterialDetails() {
        return checkMaterialDetails;
    }

    public void setCheckMaterialDetails(ArrayList checkMaterialDetails) {
        this.checkMaterialDetails = checkMaterialDetails;
    }

    public void addCheckMaterialDetail(CheckMaterialDetail checkMaterialDetail) {
        checkMaterialDetails.add(checkMaterialDetail);

    }

    public void addCheckMaterialDetail(Material material) {
        boolean isMaterialExist = false;

        for (CheckMaterialDetail cmd : checkMaterialDetails) {
            if (cmd.getMatId() == material.getId()) {
                isMaterialExist = true;
                break;
            }
        }

        if (!isMaterialExist) {
            CheckMaterialDetail cmd = new CheckMaterialDetail(material.getName(), material.getQty(), 0, 0, material.getId(), -1);
            checkMaterialDetails.add(cmd);
        }
    }

    public void delCheckMaterialDetail(ArrayList<CheckMaterialDetail> checkMaterialDetail) {
        checkMaterialDetails.remove(checkMaterialDetail);
//        calculateBillTotal();

    }

    @Override
    public String toString() {
        return "CheckMaterial{" + "checkMatId=" + checkMatId + ", checkMatDate=" + checkMatDate + ", checkMatTime=" + checkMatTime + ", userId=" + userId + ", user=" + user + '}';
    }

    public static CheckMaterial fromRS(ResultSet rs) {
        CheckMaterial checkMaterial = new CheckMaterial();
        try {
            checkMaterial.setCheckMatId(rs.getInt("check_mat_id"));
            checkMaterial.setCheckMatDate(rs.getString("check_mat_date"));
            checkMaterial.setCheckMatTime(rs.getString("check_mat_time"));
            checkMaterial.setUserId(rs.getInt("user_id"));

        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

        return checkMaterial;
    }

}
