/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kanthapong
 */
public class PosReport {
    private int id;
    private String name;
    private int Qty;
    private float TotalPrice;
    private String Month;
    private String Year;
    private int Expenses;
    private int Income;
    private int Total;

    public PosReport(int id, String name, int Qty, float TotalPrice, String Month, String Year, int Expenses, int Income, int Total) {
        this.id = id;
        this.name = name;
        this.Qty = Qty;
        this.TotalPrice = TotalPrice;
        this.Month = Month;
        this.Year = Year;
        this.Expenses = Expenses;
        this.Income = Income;
        this.Total = Total;
    }

    public PosReport(String name, int Qty, float TotalPrice, String Month, String Year, int Expenses, int Income, int Total) {
        this.id = -1;
        this.name = name;
        this.Qty = Qty;
        this.TotalPrice = TotalPrice;
        this.Month = Month;
        this.Year = Year;
        this.Expenses = Expenses;
        this.Income = Income;
        this.Total = Total;
    }

    public PosReport() {
        this(-1, "", 0,0,"","", 0, 0, 0);
    }

    public float getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(float TotalPrice) {
        this.TotalPrice = TotalPrice;
    }

    public String getMonth() {
        return Month;
    }

    public void setMonth(String Month) {
        this.Month = Month;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String Year) {
        this.Year = Year;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int Qty) {
        this.Qty = Qty;
    }

    public int getExpenses() {
        return Expenses;
    }

    public void setExpenses(int Expenses) {
        this.Expenses = Expenses;
    }

    public int getIncome() {
        return Income;
    }

    public void setIncome(int Income) {
        this.Income = Income;
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int Total) {
        this.Total = Total;
    }

    @Override
    public String toString() {
        return "PosReport{" + "id=" + id + ", name=" + name + ", Qty=" + Qty + ", TotalPrice=" + TotalPrice + ", Month=" + Month + ", Year=" + Year + ", Expenses=" + Expenses + ", Income=" + Income + ", Total=" + Total + '}';
    }
    


    
    
    public static PosReport fromRS(ResultSet rs) {
        PosReport obj = new PosReport();
        try {
            obj.setId(rs.getInt("ID"));
            obj.setName(rs.getString("Name"));
            obj.setQty(rs.getInt("Qty"));
        } catch (SQLException ex) {
            Logger.getLogger(PosReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
    
    public static PosReport fromRSS(ResultSet rs) {
        PosReport oop = new PosReport();
        try {
            oop.setMonth(rs.getString("Month"));
            oop.setExpenses(rs.getInt("Expenses"));
            oop.setIncome(rs.getInt("Income"));
            oop.setTotal(rs.getInt("Total"));
        } catch (SQLException ex) {
            Logger.getLogger(PosReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return oop;
    }
}
