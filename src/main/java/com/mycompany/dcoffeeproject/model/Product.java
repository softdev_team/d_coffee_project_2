/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chanatip
 */
public class Product {
    private int id;
    private String name;
    private String type;
    private String size;
    private float price;
    private String sweet_level;
    private int categoryId;

    public Product(int id, String name, String type, String size, float price, String sweet_level, int categoryId) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.size = size;
        this.price = price;
        this.sweet_level = sweet_level;
        this.categoryId = categoryId;
    }
    
    public Product(String name, String type, String size, float price, String sweet_level, int categoryId) {
        this.id = -1;
        this.name = name;
        this.type = type;
        this.size = size;
        this.price = price;
        this.sweet_level = sweet_level;
        this.categoryId = categoryId;
    }
    
    public Product() {
        this.id = -1;
        this.name = "";
        this.type = "";
        this.size = "";
        this.price = 0;
        this.sweet_level = "";
        this.categoryId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getSweet_level() {
        return sweet_level;
    }

    public void setSweet_level(String sweet_level) {
        this.sweet_level = sweet_level;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        try {
            product.setId(rs.getInt("product_id"));
            product.setName(rs.getString("product_name"));
            product.setType(rs.getString("product_type"));
            product.setSize(rs.getString("product_size"));
            product.setPrice(rs.getFloat("product_price"));           
            product.setSweet_level(rs.getString("product_sweetlevel"));
            product.setCategoryId(rs.getInt("category_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }
}
