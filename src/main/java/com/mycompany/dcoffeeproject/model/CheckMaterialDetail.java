/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class CheckMaterialDetail {
   private int id ;
   private String name;
   private int qtyLast;
   private int remain;
   private int qtyExpire ;
   private int matId ;
   private int checkMatId;

    public CheckMaterialDetail(int id, String name, int qtyLast, int remain, int qtyExpire, int matId, int checkMatId) {
        this.id = id;
        this.name = name;
        this.qtyLast = qtyLast;
        this.remain = remain;
        this.qtyExpire = qtyExpire;
        this.matId = matId;
        this.checkMatId = checkMatId;
    }
    
    public CheckMaterialDetail(String name, int qtyLast, int remain, int qtyExpire, int matId, int checkMatId) {
        this.id = -1;
        this.name = name;
        this.qtyLast = qtyLast;
        this.remain = remain;
        this.qtyExpire = qtyExpire;
        this.matId = matId;
        this.checkMatId = checkMatId;
    }
    
    public CheckMaterialDetail() {
        this.id = -1;
        this.name = "";
        this.qtyLast = 0;
        this.remain = 0;
        this.qtyExpire = 0;
        this.matId = 0;
        this.checkMatId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQtyLast() {
        return qtyLast;
    }

    public void setQtyLast(int qtyLast) {
        this.qtyLast = qtyLast;
    }

    public int getRemain() {
        return remain;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }

    public int getQtyExpire() {
        return qtyExpire;
    }

    public void setQtyExpire(int qtyExpire) {
        this.qtyExpire = qtyExpire;
    }

    public int getMatId() {
        return matId;
    }

    public void setMatId(int matId) {
        this.matId = matId;
    }

    public int getCheckMatId() {
        return checkMatId;
    }

    public void setCheckMatId(int checkMatId) {
        this.checkMatId = checkMatId;
    }

    @Override
    public String toString() {
        return "CheckMaterialDetail{" + "id=" + id + ", name=" + name + ", qtyLast=" + qtyLast + ", remain=" + remain + ", qtyExpire=" + qtyExpire + ", matId=" + matId + ", checkMatId=" + checkMatId + '}';
    }
  
    public static CheckMaterialDetail fromRS(ResultSet rs) {
        CheckMaterialDetail checkMaterialDetail = new CheckMaterialDetail();
        try {
            checkMaterialDetail.setId(rs.getInt("cmd_id"));
            checkMaterialDetail.setName(rs.getString("cmd_name"));
            checkMaterialDetail.setQtyLast(rs.getInt("cmd_qty_last"));
            checkMaterialDetail.setRemain(rs.getInt("cmd_remain"));
            checkMaterialDetail.setQtyExpire(rs.getInt("cmd_qty_expire"));
            checkMaterialDetail.setMatId(rs.getInt("mat_id"));
            checkMaterialDetail.setCheckMatId(rs.getInt("check_mat_id"));
            
            
            
            
        } catch (SQLException ex) {
            Logger.getLogger(CheckMaterialDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkMaterialDetail;
    }
}
