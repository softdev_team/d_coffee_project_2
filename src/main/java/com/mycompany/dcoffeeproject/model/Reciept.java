/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.CustomerDao;
import com.mycompany.dcoffeeproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chanatip
 */
public class Reciept {

    private int id;
    private Date createdDate;
    private int queue;
    private float discount;
    private float total;
    private float receieved;
    private float change;
    private String payment_name;
    private int totalQty;
    private int userId;
    private int customerId;
    private User user;
    private Customer customer;
    private ArrayList<RecieptDetail> recieptDetails = new ArrayList<RecieptDetail>();

    public Reciept(int id, Date createdDate, int queue, float discount, float total, float receieved, float change, String payment_name, int totalQty, int userId, int customerId, User user, Customer customer) {
        this.id = id;
        this.createdDate = createdDate;
        this.queue = queue;
        this.discount = discount;
        this.total = total;
        this.receieved = receieved;
        this.change = change;
        this.payment_name = payment_name;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
        this.user = user;
        this.customer = customer;
    }

    public Reciept(Date createdDate, int queue, float discount, float total, float receieved, float change, String payment_name, int totalQty, int userId, int customerId, User user, Customer customer) {
        this.id = -1;
        this.createdDate = createdDate;
        this.queue = queue;
        this.discount = discount;
        this.total = total;
        this.receieved = receieved;
        this.change = change;
        this.payment_name = payment_name;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
        this.user = user;
        this.customer = customer;
    }

    public Reciept(int queue, float discount, float total, float receieved, float change, String payment_name, int totalQty, int userId, int customerId, User user, Customer customer) {
        this.id = -1;
        this.createdDate = null;
        this.queue = queue;
        this.discount = discount;
        this.total = total;
        this.receieved = receieved;
        this.change = change;
        this.payment_name = payment_name;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
        this.user = user;
        this.customer = customer;
    }

    public Reciept(int queue, String payment_name, int userId, int customerId, User user, Customer customer) {
        this.id = -1;
        this.createdDate = null;
        this.queue = queue;
        this.discount = 0;
        this.total = 0;
        this.receieved = 0;
        this.change = 0;
        this.payment_name = payment_name;
        this.totalQty = 0;
        this.userId = userId;
        this.customerId = customerId;
    }

//    //Add Discount
//    public Reciept(int queue, String payment_name, int userId, int customerId, User user, Customer customer,int discount) {
//        this.id = -1;
//        this.createdDate = null;
//        this.queue = queue;
//        this.discount = discount;
//        this.total = 0;
//        this.receieved = 0;
//        this.change = 0;
//        this.payment_name = payment_name;
//        this.totalQty = 0;
//        this.userId = userId;
//        this.customerId = customerId;
//    }
    public Reciept() {
        this.id = -1;
        this.createdDate = null;
        this.queue = 0;
        this.discount = 0;
        this.total = 0;
        this.receieved = 0;
        this.change = 0;
        this.payment_name = "";
        this.totalQty = 0;
        this.userId = 0;
        this.customerId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getQueue() {
        return queue;
    }

    public void setQueue(int queue) {
        this.queue = queue;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getReceieved() {
        return receieved;
    }

    public void setReceieved(float receieved) {
        this.receieved = receieved;
    }

    public float getChange() {
        return change;
    }

    public void setChange(float change) {
        this.change = change;
    }

    public String getPayment_name() {
        return payment_name;
    }

    public void setPayment_name(String payment_name) {
        this.payment_name = payment_name;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ArrayList<RecieptDetail> getRecieptDetails() {
        return recieptDetails;
    }

    public void setRecieptDetails(ArrayList<RecieptDetail> recieptDetails) {
        this.recieptDetails = recieptDetails;
    }

    public void clearRecieptDetails() {
        recieptDetails.clear();
        calculateTotal();
    }

    @Override
    public String toString() {
        return "Reciept{" + "id=" + id + ", createdDate=" + createdDate + ", queue=" + queue + ", discount=" + discount + ", total=" + total + ", receieved=" + receieved + ", change=" + change + ", payment_name=" + payment_name + ", totalQty=" + totalQty + ", userId=" + userId + ", customerId=" + customerId + ", user=" + user + ", customer=" + customer + ", recieptDetails=" + recieptDetails + '}';
    }

    public void addRecieptDetail(Product product, int qty) {
        // Check if productType and productSize are not empty strings before adding the detail
        if (!product.getType().isEmpty() && !product.getSize().isEmpty()) {
            RecieptDetail rd = new RecieptDetail(product.getId(), product.getName(), product.getPrice(),
                    product.getType(), product.getSize(), qty, qty * product.getPrice(), -1);
            recieptDetails.add(rd);
            calculateTotal();
        }
    }

    public void addRecieptDetail(Product product, String productType, String productSize, int qty) {
        // Check if productType and productSize are not empty strings before adding the detail
        if (!productType.isEmpty() && !productSize.isEmpty()) {
            RecieptDetail rd = new RecieptDetail(product.getId(), product.getName(), product.getPrice(),
                    productType, productSize, qty, qty * product.getPrice(), -1);
            recieptDetails.add(rd);
            calculateTotal();
        }
    }

    public void updateRecieptDetails(Product product, String productType, String productSize, int qty) {
        boolean found = false;

        for (RecieptDetail recieptDetail : recieptDetails) {

            if (recieptDetail.getProductName().equals(product.getName())
                    && recieptDetail.getProducttSize().equals(productType)
                    && recieptDetail.getProductType().equals(productSize)) {
                // If the product with the same name, size, and type is found, update its quantity
                recieptDetail.setQty(recieptDetail.getQty() + qty);
                recieptDetail.setTotalPrice(recieptDetail.getProductPrice() * recieptDetail.getQty());
                found = true;
                break;
            }
        }

        // If the product with the specified name, size, and type was not found, add a new RecieptDetail
        if (!found) {
            // If the product doesn't exist in the receipt, add it as a new item
            addRecieptDetail(product, productSize, productType, qty);
        }

        calculateTotal(); // Recalculate the total after the update
    }

    public void delRecieptDetail(RecieptDetail recieptDetail) {
        recieptDetails.remove(recieptDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (RecieptDetail rd : recieptDetails) {
            total += rd.getTotalPrice();
            totalQty += rd.getQty();
        }
        this.totalQty = totalQty;
        this.total = total;
    }

    public void removeRecieptDetailAtIndex(int index) {
        if (index >= 0 && index < recieptDetails.size()) {
            recieptDetails.remove(index);
            calculateTotal();
        }
    }

    public static Reciept fromRS(ResultSet rs) {
        Reciept reciept = new Reciept();
        try {
            reciept.setId(rs.getInt("reciept_id"));
            reciept.setCreatedDate(rs.getTimestamp("reciept_date"));
            reciept.setQueue(rs.getInt("reciept_queue"));
            reciept.setDiscount(rs.getFloat("reciept_discount"));
            reciept.setTotal(rs.getFloat("reciept_total"));
            reciept.setReceieved(rs.getFloat("reciept_received"));
            reciept.setChange(rs.getFloat("reciept_change"));
            reciept.setPayment_name(rs.getString("payment_name"));
            reciept.setTotalQty(rs.getInt("reciept_total_qty"));
            reciept.setUserId(rs.getInt("user_id"));
            // Set the customerId to the value obtained from the ResultSet
            reciept.setCustomerId(rs.getInt("customer_id"));
            // Populate customer and user here
            CustomerDao customerDao = new CustomerDao();
            UserDao userDao = new UserDao();
            Customer customer = customerDao.get(reciept.getCustomerId());
            User user = userDao.get(reciept.getUserId());
            reciept.setCustomer(customer);
            reciept.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(Reciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }
}
