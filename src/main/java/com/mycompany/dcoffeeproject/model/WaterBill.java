/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mookda
 */
public class WaterBill {

    private int id;
    private Date date;
    private int unit;
    private float unitPrice;
    private float totalPrice;

    public WaterBill(int id, Date date, int unit, float unitPrice, float totalPrice) {
        this.id = id;
        this.date = date;
        this.unit = unit;
        this.unitPrice = unitPrice;
        this.totalPrice = totalPrice;
    }

    public WaterBill(Date date, int unit, float unitPrice, float totalPrice) {
        this.id = -1;
        this.date = date;
        this.unit = unit;
        this.unitPrice = unitPrice;
        this.totalPrice = totalPrice;
    }

    public WaterBill() {
        this.id = -1;
        this.date = null;
        this.unit = 0;
        this.unitPrice = 0.0f;
        this.totalPrice = 0.0f;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "WaterBill{" + "id=" + id + ", date=" + date + ", unit=" + unit + ", unitPrice=" + unitPrice + ", totalPrice=" + totalPrice + '}';
    }

    public static WaterBill fromRS(ResultSet rs) {
        WaterBill waterBill = new WaterBill();
        try {
            waterBill.setId(rs.getInt("water_bill_id"));
            waterBill.setDate(rs.getTimestamp("water_date"));
            waterBill.setUnit(rs.getInt("water_unit"));
            waterBill.setUnitPrice(rs.getFloat("water_unit_price"));
            waterBill.setTotalPrice(rs.getFloat("water_total_price"));
        } catch (SQLException ex) {
            Logger.getLogger(WaterBill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return waterBill;
    }
}
