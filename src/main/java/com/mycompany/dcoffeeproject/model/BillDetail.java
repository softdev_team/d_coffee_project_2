/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.BillDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USER
 */
public class BillDetail {

    private int id;
    private int matId;
    private String name;
    private int qty;
    private String unit;
    private float pricePerUnit;
    private float total;
    private int billId;
    private Bill bill;

    public BillDetail(int id, int matId, String name, int qty, String unit, float pricePerUnit, float total, int billId) {
        this.id = id;
        this.matId = matId;
        this.name = name;
        this.qty = qty;
        this.unit = unit;
        this.pricePerUnit = pricePerUnit;
        this.total = total;
        this.billId = billId;
    }

    public BillDetail(int matId, String name, int qty, String unit, float pricePerUnit, float total, int billId) {
        this.id = -1;
        this.matId = matId;
        this.name = name;
        this.qty = qty;
        this.unit = unit;
        this.pricePerUnit = pricePerUnit;
        this.total = total;
        this.billId = billId;
    }

    public BillDetail() {
        this.id = -1;
        this.matId = 0;
        this.name = "";
        this.qty = 0;
        this.unit = "";
        this.pricePerUnit = 0;
        this.total = 0;
        this.billId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatId() {
        return matId;
    }

    public void setMatId(int matId) {
        this.matId = matId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public float getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(float pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
        if (bill != null) {
            this.billId = bill.getId();
        }
    }

    @Override
    public String toString() {
        return "BillDetail{" + "id=" + id + ", matId=" + matId + ", name=" + name + ", qty=" + qty + ", unit=" + unit + ", pricePerUnit=" + pricePerUnit + ", total=" + total + ", billId=" + billId + ", bill=" + bill + '}';
    }

    public static BillDetail fromRS(ResultSet rs) {
        BillDetail billDetail = new BillDetail();
        try {
            billDetail.setId(rs.getInt("bill_detail_id"));
            billDetail.setMatId(rs.getInt("mat_id"));
            billDetail.setName(rs.getString("mat_name"));
            billDetail.setQty(rs.getInt("bill_detail_quantity"));
            billDetail.setUnit(rs.getString("mat_unit"));
            billDetail.setPricePerUnit(rs.getFloat("price_per_unit"));
            billDetail.setTotal(rs.getFloat("bill_detail_total"));
            billDetail.setBillId(rs.getInt("bill_id"));

        } catch (SQLException ex) {
            Logger.getLogger(BillDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billDetail;
    }
}
