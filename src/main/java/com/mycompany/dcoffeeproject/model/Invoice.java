/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mookda
 */
public class Invoice {

    private int id;
    private String date;
    private float total;
    private ArrayList<WaterBill> waterBills = new ArrayList<WaterBill>();
    private ArrayList<ElectricBill> electricBills = new ArrayList<ElectricBill>();
    private ArrayList<RentalBill> rentalBills = new ArrayList<RentalBill>();
    private int invoiceId;
    private int rentalId;
    private int waterId;
    private int electricId;

    public Invoice(int id, String date, float total) {
        this.id = id;
        this.date = date;
        this.total = total;
    }

    public Invoice(String date, float total) {
        this.id = -1;
        this.date = date;
        this.total = total;
    }

    public Invoice() {
        this.id = -1;
        this.date = null;
        this.total = 0.00f;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public ArrayList<WaterBill> getWaterBills() {
        return waterBills;
    }

    public void setWaterBills(ArrayList<WaterBill> waterBills) {
        this.waterBills = waterBills;
    }

    public ArrayList<ElectricBill> getElectricBills() {
        return electricBills;
    }

    public void setElectricBills(ArrayList<ElectricBill> electricBills) {
        this.electricBills = electricBills;
    }

    public ArrayList<RentalBill> getRentalBills() {
        return rentalBills;
    }

    public void setRentalBills(ArrayList<RentalBill> rentalBills) {
        this.rentalBills = rentalBills;
    }

    public int getRecieptId() {
        return invoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        this.invoiceId = invoiceId;
    }

    public int getRentalId() {
        return rentalId;
    }

    public void setRentalId(int rentalId) {
        this.rentalId = rentalId;
    }

    public int getWaterId() {
        return waterId;
    }

    public void setWaterId(int waterId) {
        this.waterId = waterId;
    }

    public int getElectricId() {
        return electricId;
    }

    public void setElectricId(int electricId) {
        this.electricId = electricId;
    }

//    @Override
//    public String toString() {
//        return "Invoice{" + "id=" + id + ", date=" + date + ", total=" + total + ", waterBills=" + waterBills + ", electricBills=" + electricBills + ", rentalBills=" + rentalBills + '}';
//    }
    @Override
    public String toString() {
        return "Invoice{" + "id=" + id + ", date=" + date + ", total=" + total + '}';
    }
    

    public static Invoice fromRS(ResultSet rs) {
        Invoice invoice = new Invoice();
        try {
            invoice.setId(rs.getInt("invoice_id"));
            invoice.setDate(rs.getString("invoice_date"));
            invoice.setTotal(rs.getFloat("invoice_total"));
            invoice.setRentalId(rs.getInt("rental_bill_id"));
            invoice.setWaterId(rs.getInt("water_bill_id"));
            invoice.setElectricId(rs.getInt("electric_bill_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Invoice.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return invoice;
    }
}
