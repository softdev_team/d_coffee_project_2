/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.ReportDao;
import com.mycompany.dcoffeeproject.model.PosReport;
import java.util.List;

/**
 *
 * @author kanthapong
 */
public class ReportService {
    public List<PosReport> getTopFiveProductByTotalQuantity() {
        ReportDao reportDao = new ReportDao();
        return reportDao.getProductByTotalQuantity(10);
    }
    public List<PosReport> getTopFiveProductByTotalQuantity(String begin, String end) {
        ReportDao reportDao = new ReportDao();
        return reportDao.getProductByTotalQuantity(begin, end, 10);
    }
    
     public List<PosReport> getTopFiveProductByTotalQuantityMin() {
        ReportDao reportDao = new ReportDao();
        return reportDao.getProductByTotalQuantityMin(10);
    }
     
     public List<PosReport> getTopFiveMatByTotalQuantityMin() {
        ReportDao reportDao = new ReportDao();
        return reportDao.getMatByTotalQuantityMin(10);
    }
     
     public List<PosReport> getMonthByTotalPrice() {
        ReportDao reportDao = new ReportDao();
        return reportDao.getMonthByTotalPrice(12);
    }
     
     public List<PosReport> getMonthByTotalPrice(String begin, String end) {
        ReportDao reportDao = new ReportDao();
        return reportDao.getMonthByTotalPrice(begin, end,12);
    }
}
