/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.WaterBillDao;
import com.mycompany.dcoffeeproject.model.WaterBill;
import java.util.List;

/**
 *
 * @author mookda
 */
public class WaterBillService {

    public WaterBill getById(int id) {
        WaterBillDao waterBillDao = new WaterBillDao();
        return waterBillDao.get(id);
    }

    public List<WaterBill> getWaterBills() {
        WaterBillDao waterBillDao = new WaterBillDao();
        return waterBillDao.getAll(" water_bill_id asc");
    }

    public WaterBill addNew(WaterBill editedWaterBill) {
        WaterBillDao waterBillDao = new WaterBillDao();
        return waterBillDao.save(editedWaterBill);
    }

    public WaterBill update(WaterBill editedWaterBill) {
        WaterBillDao waterBillDao = new WaterBillDao();
        return waterBillDao.update(editedWaterBill);
    }

    public int delete(WaterBill editedWaterBill) {
        WaterBillDao waterBillDao = new WaterBillDao();
        return waterBillDao.delete(editedWaterBill);
    }
}
