/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.CustomerDao;
import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.Customer;
import com.mycompany.dcoffeeproject.model.Promotion;
import com.mycompany.dcoffeeproject.ui.POS;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author macos
 */
public class CustomerService {

    private Connection connection;
    private POS pos;

    public Customer getByTel(String tel) {
        CustomerDao CustomerDao = new CustomerDao();
        Customer Customer = CustomerDao.getByTel(tel);
        return Customer;

    }

    public List<Customer> getCustomers() {
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.getAll(" customer_id asc");
    }

    public Customer addNew(Customer editedCustomer) {
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.save(editedCustomer);
    }

    public Customer update(Customer editedCustomer) {
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.update(editedCustomer);
    }

    public int delete(Customer editedCustomer) {
        CustomerDao CustomerDao = new CustomerDao();
        return CustomerDao.delete(editedCustomer);
    }

    public CustomerService() {
        connection = DatabaseHelper.getConnect();
    }

    public String findCustomerNameByTel(String tel) {
        String customerName = null;
        if (tel.length() != 10) {
            System.out.println("Invalid Tel");
            JOptionPane.showMessageDialog(pos, "Invalid Tel");
        } else {

            try {
                String query = "SELECT customer_name FROM customer WHERE customer_tel = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, tel);

                ResultSet rs = statement.executeQuery();
                if (rs.next()) {
                    customerName = rs.getString("customer_name");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return customerName;

    }

    public String findCustomerIdByTel(String tel) {
        String customerId = null;
        try {
            String query = "SELECT customer_id FROM customer WHERE customer_tel = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, tel);

            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                customerId = rs.getString("customer_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return customerId;
    }

    public Customer getCustomerById(int customerId) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.get(customerId);
    }

    public void managePointAfterPurchase(Customer editedCustomer, Promotion promotion) {
        if (editedCustomer != null) {
            promotion.getEarnedPoints();
            promotion.getRequiredPoints();
            System.out.println("RequiredPoint");
            editedCustomer.setPoint(editedCustomer.getPoint() - promotion.getRequiredPoints());
            System.out.println("EarnedPoint");
            editedCustomer.setPoint(editedCustomer.getPoint() + promotion.getEarnedPoints());
            CustomerDao CustomerDao = new CustomerDao();
            CustomerDao.update(editedCustomer);
        } else {
            return;
        }

    }
}
