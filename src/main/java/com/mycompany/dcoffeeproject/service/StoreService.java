/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.StoreDao;
import com.mycompany.dcoffeeproject.model.Store;
import java.util.List;

/**
 *
 * @author kanthapong
 */
public class StoreService {
    
    public List<Store> getStores(){
        StoreDao storeDao = new StoreDao();
        return storeDao.getAll(" store_name asc");
    }

    public Store addNew(Store editedStore) {
        StoreDao storeDao = new StoreDao();
        return storeDao.save(editedStore);
    }

    public Store update(Store editedStore) {
        StoreDao storeDao = new StoreDao();
        return storeDao.update(editedStore);
    }

    public int delete(Store editedStore) {
        StoreDao storeDao = new StoreDao();
        return storeDao.delete(editedStore);
    }
}
