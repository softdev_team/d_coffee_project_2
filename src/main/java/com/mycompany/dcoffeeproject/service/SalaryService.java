/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.SalaryDao;
import com.mycompany.dcoffeeproject.model.CheckInOut;
import com.mycompany.dcoffeeproject.model.Salary;
import java.util.List;

/**
 *
 * @author mookda
 */
public class SalaryService {
    

    public Salary getById(int id) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.get(id);
    }

    public List<Salary> getSalarys() {
        SalaryDao SalaryDao = new SalaryDao();
        return SalaryDao.getAll(" salary_id asc");
    }

    public Salary addNew(Salary editedSalar, List<CheckInOut> cios ) {
        SalaryDao SalaryDao = new SalaryDao();
        CheckInOutService check = new CheckInOutService();
         Salary salary =SalaryDao.save(editedSalar);
        for(CheckInOut cio : cios){
            cio.setSalaryId(salary.getId());
            check.update(cio);
        }
        return salary;
        
    }

    public Salary update(Salary editedSalary) {
        SalaryDao SalaryDao = new SalaryDao();
        return SalaryDao.update(editedSalary);
    }

    public int delete(Salary editedSalary) {
        SalaryDao SalaryDao = new SalaryDao();
        return SalaryDao.delete(editedSalary);
    }
}
