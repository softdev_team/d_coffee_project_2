/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.CheckInOutDao;
import com.mycompany.dcoffeeproject.model.CheckInOut;
import java.util.List;

/**
 *
 * @author mookda
 */
public class CheckInOutService {
    
   private CheckInOutDao checkInOutDao = new CheckInOutDao();

    public CheckInOut getById(int id) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.get(id);
    }

    public List<CheckInOut> getCheckInOuts() {
        CheckInOutDao CheckInOutDao = new CheckInOutDao();
        return CheckInOutDao.getAll(" cio_id asc");
    }

    public List<CheckInOut> getCheckInOutsByUserId(int userId) {
        CheckInOutDao CheckInOutDao = new CheckInOutDao();
        return CheckInOutDao.getByUserId(userId);
    }
    
      public List<CheckInOut> getCheckByUserId(int userId) {
        CheckInOutDao CheckInOutDao = new CheckInOutDao();
        return CheckInOutDao.getAllByUserId(userId);
    }
    

    public CheckInOut addNew(CheckInOut editedCheckInOut) {
        CheckInOutDao CheckInOutDao = new CheckInOutDao();
        return CheckInOutDao.save(editedCheckInOut);
    }

    public CheckInOut update(CheckInOut editedCheckInOut) {
        CheckInOutDao CheckInOutDao = new CheckInOutDao();
        return CheckInOutDao.update(editedCheckInOut);
 // ตรวจสอบว่าค่า paid ถูกตั้งค่าเป็น true หรือไม่
//        if (editedCheckInOut.isPaid()) {
//            // ถ้าถูกตั้งค่าเป็น true ให้อัปเดตฐานข้อมูล
//            editedCheckInOut = checkInOutDao.update(editedCheckInOut);
//        } else {
//            // ถ้าไม่ถูกตั้งค่าเป็น true ให้ไม่ทำอะไร
//        }
//        return editedCheckInOut;
    }

    public int delete(CheckInOut editedCheckInOut) {
        CheckInOutDao CheckInOutDao = new CheckInOutDao();
        return CheckInOutDao.delete(editedCheckInOut);
    }

}
