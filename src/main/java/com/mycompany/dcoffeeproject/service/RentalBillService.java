/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.RentalBillDao;
import com.mycompany.dcoffeeproject.model.RentalBill;
import java.util.List;

/**
 *
 * @author mookda
 */
public class RentalBillService {

    public RentalBill getById(int id) {
        RentalBillDao rentalBillDao = new RentalBillDao();
        return rentalBillDao.get(id);
    }

    public List<RentalBill> getRentalBills() {
        RentalBillDao rentalBillDao = new RentalBillDao();
        return rentalBillDao.getAll(" rental_bill_id asc");
    }

    public RentalBill addNew(RentalBill editedRentalBill) {
        RentalBillDao rentalBillDao = new RentalBillDao();
        return rentalBillDao.save(editedRentalBill);
    }

    public RentalBill update(RentalBill editedRentalBill) {
        RentalBillDao rentalBillDao = new RentalBillDao();
        return rentalBillDao.update(editedRentalBill);
    }

    public int delete(RentalBill editedRentalBill) {
        RentalBillDao rentalBillDao = new RentalBillDao();
        return rentalBillDao.delete(editedRentalBill);
    }
}
