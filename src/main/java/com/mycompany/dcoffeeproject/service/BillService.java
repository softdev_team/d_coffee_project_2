/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.BillDao;
import com.mycompany.dcoffeeproject.dao.BillDetailDao;
import com.mycompany.dcoffeeproject.dao.MaterialDao;
import com.mycompany.dcoffeeproject.model.Bill;
import com.mycompany.dcoffeeproject.model.BillDetail;
import com.mycompany.dcoffeeproject.model.Material;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USER
 */
public class BillService {

    public Bill getById(int id) {
        BillDao billDao = new BillDao();
        return billDao.get(id);
    }

    public List<Bill> getBills() {
        BillDao billDao = new BillDao();
        return billDao.getAll(" bill_id asc");
    }

    public Bill addNew(Bill editedBill) {
        BillDao billDao = new BillDao();
        BillDetailDao billDetailDao = new BillDetailDao();
        MaterialDao materialDao = new MaterialDao();
        Bill bill = billDao.save(editedBill);
        //save one by one
        for (BillDetail bd : editedBill.getBillDetails()) {
            bd.setBillId(bill.getId());
            billDetailDao.save(bd);

            Material material = materialDao.get(bd.getMatId());

            if (material != null) {

                int newQuantity = material.getQty() + bd.getQty();
                material.setQty(newQuantity);

                // Update unit and pricePerUnit
                material.setUnit(bd.getUnit());
                material.setPricePerUnit(bd.getPricePerUnit());
                materialDao.update(material);

            }

        }

        return bill;
    }

    public List<Bill> getBillsWithDetails() {
        BillDao billDao = new BillDao();
        List<Bill> bills = billDao.getAll();

        BillDetailDao billDetailDao = new BillDetailDao();
        for (Bill bill : bills) {
            List<BillDetail> billDetails = billDetailDao.getAll();
            bill.setBillDetails(new ArrayList<>(billDetails));
        }

        return bills;
    }

    public Bill update(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.update(editedBill);
    }

    public int delete(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.delete(editedBill);
    }
}
