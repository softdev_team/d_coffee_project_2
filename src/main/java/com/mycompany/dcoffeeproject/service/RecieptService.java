/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.RecieptDao;
import com.mycompany.dcoffeeproject.dao.RecieptDetailDao;
import com.mycompany.dcoffeeproject.dao.UserDao;
import com.mycompany.dcoffeeproject.model.Customer;
import com.mycompany.dcoffeeproject.model.Promotion;
import com.mycompany.dcoffeeproject.model.Reciept;
import com.mycompany.dcoffeeproject.model.RecieptDetail;
import com.mycompany.dcoffeeproject.model.User;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author chanatip
 */
public class RecieptService {

    private PromotionService promotionService;
    private Promotion promotion;
//    private RecieptDetailDao recieptDetailDao;

    public Reciept getById(int id) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.get(id);
    }

    public List<RecieptDetail> getRecieptDetailByRecieptId(int id) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.getByRecieptId(id);
    }

    public List<Reciept> getReciepts() {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getAll(" reciept_id desc");
    }

    public Reciept addNew(Reciept editedReciept, Promotion promotion) {
        RecieptDao recieptDao = new RecieptDao();
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();

        if (editedReciept.getPayment_name().equals("QR code")) {
            if (promotion == null) {
                editedReciept.setTotal(editedReciept.getTotal());
                // For "QR code" payment, always save the receipt
                editedReciept.setReceieved(editedReciept.getTotal());
                editedReciept.setDiscount(0);
                editedReciept.setChange(0);
                Reciept reciept = recieptDao.save(editedReciept);
                for (RecieptDetail rd : editedReciept.getRecieptDetails()) {
                    rd.setRecieptId(reciept.getId());
                    recieptDetailDao.save(rd);
                }
                return reciept;
            } else {
                editedReciept.setTotal(editedReciept.getTotal());
                // For "QR code" payment, always save the receipt
                editedReciept.setReceieved(editedReciept.getTotal() - promotion.getDiscount());
                editedReciept.setDiscount(promotion.getDiscount());
                editedReciept.setChange(0);
                Reciept reciept = recieptDao.save(editedReciept);
                for (RecieptDetail rd : editedReciept.getRecieptDetails()) {
                    rd.setRecieptId(reciept.getId());
                    recieptDetailDao.save(rd);
                }
                return reciept;
            }
        } else {
            if (promotion == null) {
                editedReciept.setTotal(editedReciept.getTotal());
                // For "QR code" payment, always save the receipt
                editedReciept.setDiscount(0);
                editedReciept.setChange(editedReciept.getReceieved() - editedReciept.getTotal());
                Reciept reciept = recieptDao.save(editedReciept);
                for (RecieptDetail rd : editedReciept.getRecieptDetails()) {
                    rd.setRecieptId(reciept.getId());
                    recieptDetailDao.save(rd);
                }
                return reciept;
            } else {
                editedReciept.setTotal(editedReciept.getTotal());
                // For "QR code" payment, always save the receipt
                editedReciept.setDiscount(promotion.getDiscount());
                editedReciept.setChange(editedReciept.getReceieved() - (editedReciept.getTotal() - editedReciept.getDiscount()));
                System.out.println(editedReciept.getReceieved());
                Reciept reciept = recieptDao.save(editedReciept);
                for (RecieptDetail rd : editedReciept.getRecieptDetails()) {
                    rd.setRecieptId(reciept.getId());
                    recieptDetailDao.save(rd);
                }
                return reciept;
            }

        }
    }

    public Reciept update(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.update(editedReciept);
    }

    public int delete(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.delete(editedReciept);
    }

    public boolean checkCashEnough(Reciept editedReciept, Promotion promotion) {
        if (promotion != null) {
            if (editedReciept.getReceieved() < editedReciept.getTotal() - promotion.getDiscount() && editedReciept.getPayment_name().equals("Cash")) {
                JOptionPane.showMessageDialog(null, "Not Enough Money", "Alert", JOptionPane.ERROR_MESSAGE);
                return false;
            } else {
                return true;
            }
        } else {
            if (editedReciept.getReceieved() < editedReciept.getTotal() && editedReciept.getPayment_name().equals("Cash")) {
                JOptionPane.showMessageDialog(null, "Not Enough Money", "Alert", JOptionPane.ERROR_MESSAGE);
                return false;
            } else {
                return true;
            }
        }

    }

    public boolean checkEmptyCart(Reciept editedReciept) {

        if (editedReciept.getTotal() <= 0) {
            JOptionPane.showMessageDialog(null, " Cart Is Empty", "Alert", JOptionPane.ERROR_MESSAGE);

            return false;
        } else {
            return true;
        }
    }

}
