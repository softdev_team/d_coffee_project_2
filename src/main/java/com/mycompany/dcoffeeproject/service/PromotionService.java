/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.PromotionDao;
import com.mycompany.dcoffeeproject.model.Customer;
import com.mycompany.dcoffeeproject.model.Promotion;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author chanatip
 */
public class PromotionService {

    private PromotionDao promotionDao;

    public PromotionService() {
        this.promotionDao = new PromotionDao();
    }

    public Promotion getPromotion(int id) {
        return promotionDao.get(id);
    }

    public List<Promotion> getAllPromotions() {
        return promotionDao.getAll();
    }

    public Promotion savePromotion(Promotion promotion) {
        return promotionDao.save(promotion);
    }

    public Promotion updatePromotion(Promotion promotion) {
        return promotionDao.update(promotion);
    }

    public int deletePromotion(Promotion promotion) {
        return promotionDao.delete(promotion);
    }

    public List<Promotion> searchPromotions(String where, String order) {
        return promotionDao.getAll(where, order);
    }

    // check promotion and customer point
    public boolean isEnoughPoint(Customer editedCustomer, Promotion promotion) {
        int cuspoint;
        int requiredPoint;
        cuspoint = editedCustomer.getPoint();
        requiredPoint = promotion.getRequiredPoints();
        if (cuspoint < requiredPoint) {
            System.out.println("Not Enough");
            JOptionPane.showMessageDialog(null, "Not Enough Points for the Promotion", "Point Insufficient", JOptionPane.WARNING_MESSAGE);
            return false;
        } else {
            System.out.println("Enough");
            return true;
        }

    }

}
