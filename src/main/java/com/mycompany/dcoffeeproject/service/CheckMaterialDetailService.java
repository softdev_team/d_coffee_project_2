/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.CheckMaterialDetailDao;
import com.mycompany.dcoffeeproject.model.CheckMaterialDetail;
import java.util.List;

/**
 *
 * @author User
 */
public class CheckMaterialDetailService {

    public CheckMaterialDetail getById(int id) {
        CheckMaterialDetailDao checkMaterialDetailDao = new CheckMaterialDetailDao();
        return checkMaterialDetailDao.get(id);
    }

    public List<CheckMaterialDetail> getAllCheckMaterialDetailByCheckId(int id) {
        CheckMaterialDetailDao checkMaterialDetailDao = new CheckMaterialDetailDao();
        return checkMaterialDetailDao.getAllCheckMaterialDetailByCheckId(id);
    }

    public List<CheckMaterialDetail> getCheckMaterialDetails() {
        CheckMaterialDetailDao CheckMaterialDetailDao = new CheckMaterialDetailDao();
        return CheckMaterialDetailDao.getAll(" cmd_id asc");
    }

    public CheckMaterialDetail addNew(CheckMaterialDetail editedCheckMaterialDetail) {
        CheckMaterialDetailDao CheckMaterialDetailDao = new CheckMaterialDetailDao();
        return CheckMaterialDetailDao.save(editedCheckMaterialDetail);
    }

    public CheckMaterialDetail update(CheckMaterialDetail editedCheckMaterialDetail) {
        CheckMaterialDetailDao CheckMaterialDetailDao = new CheckMaterialDetailDao();
        return CheckMaterialDetailDao.update(editedCheckMaterialDetail);
    }

    public int delete(CheckMaterialDetail editedCheckMaterialDetail) {
        CheckMaterialDetailDao CheckMaterialDetailDao = new CheckMaterialDetailDao();
        return CheckMaterialDetailDao.delete(editedCheckMaterialDetail);
    }

    public int deleteByCheckMatId(int Id) {
        CheckMaterialDetailDao CheckMaterialDetailDao = new CheckMaterialDetailDao();
        return CheckMaterialDetailDao.deleteByCheckMatId(Id);
    }

    public void saveCheckMaterialDetails(List<CheckMaterialDetail> checkMaterialDetails) {
        CheckMaterialDetailDao CheckMaterialDetailDao = new CheckMaterialDetailDao();
        for (CheckMaterialDetail checkMaterialDetail : checkMaterialDetails) {
            CheckMaterialDetailDao.save(checkMaterialDetail);
        }
    }
}
