/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.MaterialDao;
import com.mycompany.dcoffeeproject.model.Material;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USER
 */
public class MaterialService {

    public List<Material> getMaterials() {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.getAll(" mat_id asc");
    }

    public Material getByName(String name) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.getByName(name);
    }

    public ArrayList<Material> getMaterialsOrderbyName(String toString) {
        MaterialDao materialDao = new MaterialDao();
        return (ArrayList<Material>) materialDao.getAll(" mat_name ASC ");
    }

    public Material addNew(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.save(editedMaterial);
    }

    public Material update(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.update(editedMaterial);
    }

    public int delete(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.delete(editedMaterial);
    }

    public ArrayList<Material> getMaterialsOrderbyName1() {
        MaterialDao materialDao = new MaterialDao();
        return (ArrayList<Material>) materialDao.getAll(" mat_name ASC ");
    }
}
