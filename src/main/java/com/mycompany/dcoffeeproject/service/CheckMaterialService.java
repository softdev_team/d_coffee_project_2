/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.model.CheckMaterial;
import com.mycompany.dcoffeeproject.dao.CheckMaterialDao;
import com.mycompany.dcoffeeproject.dao.CheckMaterialDetailDao;
import com.mycompany.dcoffeeproject.model.CheckMaterialDetail;
import java.util.List;
import com.mycompany.dcoffeeproject.dao.MaterialDao;
import com.mycompany.dcoffeeproject.model.Material;

/**
 *
 * @author kewwy
 */
public class CheckMaterialService {
    
    public List<CheckMaterial> getCheckMaterials() {
        CheckMaterialDao checkMaterialDao = new CheckMaterialDao();
        return checkMaterialDao.getAll(" check_mat_id asc");
    }
    
    public CheckMaterial addNew(CheckMaterial editedCheckMaterial) {
        CheckMaterialDao checkMaterialDao = new CheckMaterialDao();
        CheckMaterialDetailDao cmdDao = new CheckMaterialDetailDao();
        MaterialDao materialDao = new MaterialDao();
        
        CheckMaterial checkMaterial = checkMaterialDao.save(editedCheckMaterial);
        
        for (CheckMaterialDetail cmd : editedCheckMaterial.getCheckMaterialDetails()) {
            cmd.setCheckMatId(checkMaterial.getCheckMatId());
            cmdDao.save(cmd);
            
            Material material = materialDao.get(cmd.getMatId());
            
            if (material != null) {
                material.setQty(cmd.getRemain());
                materialDao.update(material);
            }
        }
        return checkMaterial;
    }
    
    public CheckMaterial update(CheckMaterial editedCheckMaterial) {
        CheckMaterialDao checkMaterialDao = new CheckMaterialDao();
        return checkMaterialDao.update(editedCheckMaterial);
    }
    
    public int delete(CheckMaterial editedCheckMaterial) {
        CheckMaterialDao checkMaterialDao = new CheckMaterialDao();
        return checkMaterialDao.delete(editedCheckMaterial);
    }
    
    public int deleteById(int Id) {
        CheckMaterialDao checkMaterialDao = new CheckMaterialDao();
        return checkMaterialDao.deleteById(Id);
    }
    
    public int findLastId() {
        CheckMaterialDao checkMaterialDao = new CheckMaterialDao();
        return checkMaterialDao.findLastId();
    }
}
