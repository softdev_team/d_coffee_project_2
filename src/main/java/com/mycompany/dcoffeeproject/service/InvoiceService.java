/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.ElectricBillDao;
import com.mycompany.dcoffeeproject.dao.InvoiceDao;
import com.mycompany.dcoffeeproject.dao.RentalBillDao;
import com.mycompany.dcoffeeproject.dao.WaterBillDao;
import com.mycompany.dcoffeeproject.model.ElectricBill;
import com.mycompany.dcoffeeproject.model.Invoice;
import com.mycompany.dcoffeeproject.model.RentalBill;
import com.mycompany.dcoffeeproject.model.WaterBill;
import java.util.List;

/**
 *
 * @author mookda
 */
public class InvoiceService {

    public Invoice getById(int id) {
        InvoiceDao invoiceDao = new InvoiceDao();
        return invoiceDao.get(id);
    }

    public List<Invoice> getInvoices() {
        InvoiceDao invoiceDao = new InvoiceDao();
        return invoiceDao.getAll(" invoice_id asc");
    }

    public Invoice addNew(Invoice editedInvoice, RentalBill editedRentalBill, WaterBill editedWaterBill, ElectricBill editedElectricBill) {
        InvoiceDao invoiceDao = new InvoiceDao();
        RentalBillDao rentalBillDao = new RentalBillDao();
        WaterBillDao waterBillDao = new WaterBillDao();
        ElectricBillDao electricBillDao = new ElectricBillDao();
        System.out.println(editedRentalBill);
        System.out.println(editedWaterBill);
        System.out.println(editedElectricBill);

        RentalBill rentalBill = rentalBillDao.save(editedRentalBill);
        WaterBill waterBill = waterBillDao.save(editedWaterBill);
        ElectricBill electricBill = electricBillDao.save(editedElectricBill);
        editedInvoice.setRentalId(rentalBill.getId());
        editedInvoice.setWaterId(waterBill.getId());
        editedInvoice.setElectricId(electricBill.getId());

        Invoice invoice = invoiceDao.save(editedInvoice);
        System.out.println(editedInvoice);
        return editedInvoice;
    }

    public Invoice update(Invoice editedInvoice) {
        InvoiceDao invoiceDao = new InvoiceDao();
        return invoiceDao.update(editedInvoice);
    }

    public int delete(Invoice editedInvoice) {
        InvoiceDao invoiceDao = new InvoiceDao();
        return invoiceDao.delete(editedInvoice);
    }
}
