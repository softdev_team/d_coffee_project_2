/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.CategoryDao;
import com.mycompany.dcoffeeproject.model.Category;
import java.util.List;

/**
 *
 * @author mookda
 */
public class CategoryService {

    public Category getById(int id) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.get(id);
    }

    public Category getByName(String name) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.getByName(name);
    }

    public List<Category> getCategories() {
        CategoryDao CategoryDao = new CategoryDao();
        return CategoryDao.getAll(" category_id asc");
    }

    public Category addNew(Category editedCategory) {
        CategoryDao CategoryDao = new CategoryDao();
        return CategoryDao.save(editedCategory);
    }

    public Category update(Category editedCategory) {
        CategoryDao CategoryDao = new CategoryDao();
        return CategoryDao.update(editedCategory);
    }

    public int delete(Category editedCategory) {
        CategoryDao CategoryDao = new CategoryDao();
        return CategoryDao.delete(editedCategory);
    }
}
