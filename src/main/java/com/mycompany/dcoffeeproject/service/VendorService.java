/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.VendorDao;
import com.mycompany.dcoffeeproject.model.Vendor;
import java.util.List;

/**
 *
 * @author mookda
 */
public class VendorService {

    public Vendor getByName(String name) {
        VendorDao vendorDao = new VendorDao();
        return vendorDao.getByName(name);
    }

    public List<Vendor> getVendors() {
        VendorDao vendorDao = new VendorDao();
        return vendorDao.getAll(" vendor_name asc");
    }

    public Vendor addNew(Vendor editedVendor) {
        VendorDao vendorDao = new VendorDao();
        return vendorDao.save(editedVendor);
    }

    public Vendor update(Vendor editedVendor) {
        VendorDao vendorDao = new VendorDao();
        return vendorDao.update(editedVendor);
    }

    public int delete(Vendor editedVendor) {
        VendorDao vendorDao = new VendorDao();
        return vendorDao.delete(editedVendor);
    }
}
