/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.ElectricBillDao;
import com.mycompany.dcoffeeproject.model.ElectricBill;
import java.util.List;

/**
 *
 * @author mookda
 */
public class ElectricBillService {

    public ElectricBill getById(int id) {
        ElectricBillDao electricBillDao = new ElectricBillDao();
        return electricBillDao.get(id);
    }

    public List<ElectricBill> getElectricBills() {
        ElectricBillDao electricBillDao = new ElectricBillDao();
        return electricBillDao.getAll(" electric_bill_id asc");
    }

    public ElectricBill addNew(ElectricBill editedElectricBill) {
        ElectricBillDao electricBillDao = new ElectricBillDao();
        return electricBillDao.save(editedElectricBill);
    }

    public ElectricBill update(ElectricBill editedElectricBill) {
        ElectricBillDao electricBillDao = new ElectricBillDao();
        return electricBillDao.update(editedElectricBill);
    }

    public int delete(ElectricBill editedElectricBill) {
        ElectricBillDao electricBillDao = new ElectricBillDao();
        return electricBillDao.delete(editedElectricBill);
    }
}
