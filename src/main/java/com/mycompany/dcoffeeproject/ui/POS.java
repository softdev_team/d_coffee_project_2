/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui;

import com.mycompany.dcoffeeproject.component.BakeryListPanel;
import com.mycompany.dcoffeeproject.component.BuyProductable;
import com.mycompany.dcoffeeproject.component.FoodsItemPanel;
import com.mycompany.dcoffeeproject.component.FoodsListPanel;
import com.mycompany.dcoffeeproject.component.ProductListPanel;
import com.mycompany.dcoffeeproject.model.Customer;
import com.mycompany.dcoffeeproject.model.Product;
import com.mycompany.dcoffeeproject.model.Promotion;
import com.mycompany.dcoffeeproject.model.Reciept;
import com.mycompany.dcoffeeproject.model.RecieptDetail;
import com.mycompany.dcoffeeproject.model.User;
import com.mycompany.dcoffeeproject.service.CustomerService;
import com.mycompany.dcoffeeproject.service.ProductService;
import com.mycompany.dcoffeeproject.service.PromotionService;
import com.mycompany.dcoffeeproject.service.RecieptService;
import com.mycompany.dcoffeeproject.service.UserService;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author chanatip
 */
public class POS extends javax.swing.JPanel implements BuyProductable {

    ArrayList<Product> products;
    ProductService productService = new ProductService();
    RecieptService recieptService = new RecieptService();
    Reciept reciept;
    UserService userService = new UserService();
    PromotionService promotionService = new PromotionService();
    Promotion promotion;
    Customer customer;
    private final ProductListPanel productListPanel;
    private final FoodsListPanel foodsListPanel;
    private final BakeryListPanel bakeryListPanel;
    private final CustomerService customerService;
    private List<Customer> list;
    private Customer editedCustomer;
    private int customerId;
    private float overallTotal;

    /**
     * Creates new form POS
     */
    public POS() {
        initComponents();
        scrProductList.setBackground(Color.ORANGE);
        cmbPromotion.setVisible(false);
        reciept = new Reciept();
        reciept.setUser(userService.getCurrent());
        reciept.setPayment_name("Cash");
        txtCash.setText("0");
        customerService = new CustomerService();
        list = customerService.getCustomers();
        tblRecieptDetail.setModel(new AbstractTableModel() {
            String[] header = {"Name", "Price", "Qty", "Total", "Type", "Size"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return reciept.getRecieptDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return recieptDetail.getProductName();
                    case 1:
                        return recieptDetail.getProductPrice();
                    case 2:
                        return recieptDetail.getQty();
                    case 3:
                        return recieptDetail.getTotalPrice();
                    case 4:
                        return recieptDetail.getProductType();
                    case 5:
                        return recieptDetail.getProducttSize();
                    default:
                        return "";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                if (columnIndex == 2) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    recieptDetail.setQty(qty);

                    float totalPrice = recieptDetail.getProductPrice() * qty;
                    recieptDetail.setTotalPrice(totalPrice);

                    refreshReciept();
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;
                    default:
                        return false;
                }
            }
        });
        productListPanel = new ProductListPanel();
        productListPanel.addOnBuyProduct(this);
        scrProductList.setViewportView(productListPanel);

        foodsListPanel = new FoodsListPanel();
        foodsListPanel.addOnBuyProduct(this);

        bakeryListPanel = new BakeryListPanel();
        bakeryListPanel.addOnBuyProduct(this);

    }

    private void refreshReciept() {
        tblRecieptDetail.revalidate();
        tblRecieptDetail.repaint();
        // Calculate the overall total by summing up the total prices of all items
        overallTotal = 0;
        for (RecieptDetail recieptDetail : reciept.getRecieptDetails()) {
            overallTotal += recieptDetail.getTotalPrice();
        }

        reciept.setTotal(overallTotal);
        lblTotal.setText("" + overallTotal); // Update the total label
        // Calculate VAT
        float vat = overallTotal * 0.07f;
        DecimalFormat decimalFormat = new DecimalFormat("#0.00"); // Format to 2 decimal places
        String formattedVAT = decimalFormat.format(vat);
        lblVATonPos.setText(formattedVAT);
//        
//        lblStatus.setText("Non-Member");
//        lblPoint.setText("0");
//        txtMember.setText("");    
//        cmbPromotion.setSelectedIndex(0);
//        cmbPromotion.setVisible(false);
//        lblDiscount.setText("0.00");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        RecieptDia = new javax.swing.JDialog();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        recieptArea = new javax.swing.JTextArea();
        jPanel7 = new javax.swing.JPanel();
        total = new javax.swing.JLabel();
        Totallbl = new javax.swing.JLabel();
        txtPayment = new javax.swing.JLabel();
        paymentName = new javax.swing.JLabel();
        txtReceived = new javax.swing.JLabel();
        lblRecieptReceived = new javax.swing.JLabel();
        txtChange = new javax.swing.JLabel();
        lblRecieptChange = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblRecieptMember = new javax.swing.JLabel();
        vat = new javax.swing.JLabel();
        lblVat = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lblEmpName = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnDrinks = new javax.swing.JButton();
        btnFoods = new javax.swing.JButton();
        btnBakery = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        scrProductList = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        Total = new javax.swing.JLabel();
        Discount = new javax.swing.JLabel();
        VAT = new javax.swing.JLabel();
        lblPayment = new javax.swing.JLabel();
        lblCash = new javax.swing.JLabel();
        txtCash = new javax.swing.JTextField();
        cmdPayment = new javax.swing.JComboBox<>();
        lblMemberStatus = new javax.swing.JLabel();
        btnRegister = new javax.swing.JButton();
        btnMember = new javax.swing.JButton();
        lblStatus = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        lblVATonPos = new javax.swing.JLabel();
        lblMember1 = new javax.swing.JLabel();
        txtMember = new javax.swing.JTextField();
        btnReciept1 = new javax.swing.JButton();
        cmbPromotion = new javax.swing.JComboBox<>();
        Point = new javax.swing.JLabel();
        lblPoint = new javax.swing.JLabel();
        btnReciept = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnPay = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblRecieptDetail = new javax.swing.JTable();

        RecieptDia.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                RecieptDiaWindowActivated(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                RecieptDiaWindowClosing(evt);
            }
        });

        recieptArea.setEditable(false);
        recieptArea.setColumns(20);
        recieptArea.setRows(5);
        jScrollPane2.setViewportView(recieptArea);

        total.setText("Total :");

        Totallbl.setText("0.00");

        txtPayment.setText("Payment :");

        paymentName.setText("--");

        txtReceived.setText("Received :");

        lblRecieptReceived.setText("0.00");

        txtChange.setText("Change :");

        lblRecieptChange.setText("0.00");

        jLabel6.setText("Member :");

        lblRecieptMember.setText("--");

        vat.setText("Vat :");

        lblVat.setText("0.00");

        jLabel7.setText("Employee Name :");

        lblEmpName.setText("--");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(total, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 186, Short.MAX_VALUE)
                        .addComponent(Totallbl, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addComponent(txtPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(paymentName, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(txtReceived, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblRecieptReceived, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(txtChange, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblRecieptChange, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblRecieptMember, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(vat, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 186, Short.MAX_VALUE)
                        .addComponent(lblVat, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblEmpName, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(total)
                    .addComponent(Totallbl))
                .addGap(5, 5, 5)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(vat)
                    .addComponent(lblVat))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPayment)
                    .addComponent(paymentName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtReceived)
                    .addComponent(lblRecieptReceived))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtChange)
                    .addComponent(lblRecieptChange))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(lblRecieptMember))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(lblEmpName))
                .addGap(24, 24, 24))
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 318, Short.MAX_VALUE)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout RecieptDiaLayout = new javax.swing.GroupLayout(RecieptDia.getContentPane());
        RecieptDia.getContentPane().setLayout(RecieptDiaLayout);
        RecieptDiaLayout.setHorizontalGroup(
            RecieptDiaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RecieptDiaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        RecieptDiaLayout.setVerticalGroup(
            RecieptDiaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RecieptDiaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        setBackground(new java.awt.Color(48, 8, 40));

        jPanel2.setBackground(new java.awt.Color(48, 8, 40));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        btnDrinks.setBackground(new java.awt.Color(88, 72, 72));
        btnDrinks.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 14)); // NOI18N
        btnDrinks.setForeground(new java.awt.Color(245, 109, 8));
        btnDrinks.setText("Drinks");
        btnDrinks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDrinksActionPerformed(evt);
            }
        });

        btnFoods.setBackground(new java.awt.Color(88, 72, 72));
        btnFoods.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 14)); // NOI18N
        btnFoods.setForeground(new java.awt.Color(245, 109, 8));
        btnFoods.setText("Foods");
        btnFoods.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFoodsActionPerformed(evt);
            }
        });

        btnBakery.setBackground(new java.awt.Color(88, 72, 72));
        btnBakery.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 14)); // NOI18N
        btnBakery.setForeground(new java.awt.Color(245, 109, 8));
        btnBakery.setText("Bakery");
        btnBakery.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBakeryActionPerformed(evt);
            }
        });

        jPanel5.setBackground(new java.awt.Color(48, 8, 40));

        scrProductList.setBackground(new java.awt.Color(248, 128, 0));
        scrProductList.setForeground(new java.awt.Color(248, 128, 0));
        scrProductList.setOpaque(false);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrProductList, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrProductList, javax.swing.GroupLayout.DEFAULT_SIZE, 537, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(125, 125, 125)
                .addComponent(btnDrinks, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnFoods, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnBakery, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(130, Short.MAX_VALUE))
            .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDrinks, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFoods, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBakery, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(48, 8, 40));
        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        Total.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        Total.setForeground(new java.awt.Color(245, 109, 8));
        Total.setText("Total");

        Discount.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        Discount.setForeground(new java.awt.Color(245, 109, 8));
        Discount.setText("Discount");

        VAT.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        VAT.setForeground(new java.awt.Color(245, 109, 8));
        VAT.setText("VAT 7%");

        lblPayment.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        lblPayment.setForeground(new java.awt.Color(245, 109, 8));
        lblPayment.setText("Payment");

        lblCash.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        lblCash.setForeground(new java.awt.Color(245, 109, 8));
        lblCash.setText("Cash");

        txtCash.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCashActionPerformed(evt);
            }
        });

        cmdPayment.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Cash", "QR code" }));
        cmdPayment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdPaymentActionPerformed(evt);
            }
        });

        lblMemberStatus.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        lblMemberStatus.setForeground(new java.awt.Color(245, 109, 8));
        lblMemberStatus.setText("Member Status");

        btnRegister.setBackground(new java.awt.Color(255, 255, 102));
        btnRegister.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        btnRegister.setText("Register");
        btnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterActionPerformed(evt);
            }
        });

        btnMember.setBackground(new java.awt.Color(255, 51, 255));
        btnMember.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        btnMember.setText("History");
        btnMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMemberActionPerformed(evt);
            }
        });

        lblStatus.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        lblStatus.setForeground(new java.awt.Color(245, 109, 8));
        lblStatus.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblStatus.setText("Non-Member");

        lblTotal.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        lblTotal.setForeground(new java.awt.Color(245, 109, 8));
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotal.setText("0.00");

        lblDiscount.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        lblDiscount.setForeground(new java.awt.Color(245, 109, 8));
        lblDiscount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDiscount.setText("0.00");

        lblVATonPos.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        lblVATonPos.setForeground(new java.awt.Color(245, 109, 8));
        lblVATonPos.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblVATonPos.setText("0.00");

        lblMember1.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        lblMember1.setForeground(new java.awt.Color(245, 109, 8));
        lblMember1.setText("Member");

        txtMember.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMemberActionPerformed(evt);
            }
        });

        btnReciept1.setBackground(new java.awt.Color(153, 255, 153));
        btnReciept1.setText("OK");
        btnReciept1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReciept1ActionPerformed(evt);
            }
        });

        cmbPromotion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "pro member", "pro point" }));
        cmbPromotion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbPromotionActionPerformed(evt);
            }
        });

        Point.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        Point.setForeground(new java.awt.Color(245, 109, 8));
        Point.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Point.setText("Point:");

        lblPoint.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        lblPoint.setForeground(new java.awt.Color(245, 109, 8));
        lblPoint.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblPoint.setText("0");

        btnReciept.setBackground(new java.awt.Color(255, 153, 0));
        btnReciept.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        btnReciept.setText("Reciept");
        btnReciept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRecieptActionPerformed(evt);
            }
        });

        btnDelete.setBackground(new java.awt.Color(255, 102, 102));
        btnDelete.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnPay.setBackground(new java.awt.Color(153, 255, 153));
        btnPay.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        btnPay.setText("Pay");
        btnPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPayActionPerformed(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(255, 153, 153));
        btnClear.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblMemberStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Discount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Total, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(VAT, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblPayment, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblMember1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblCash, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbPromotion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblVATonPos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblDiscount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblStatus, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                            .addComponent(txtMember, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(btnReciept1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(txtCash, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(cmdPayment, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnReciept, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnClear, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnPay, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(btnRegister, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnMember, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(Point, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(53, 53, 53)
                        .addComponent(lblPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Total)
                    .addComponent(lblTotal))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Discount)
                    .addComponent(lblDiscount))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(VAT)
                    .addComponent(lblVATonPos))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPayment)
                    .addComponent(cmdPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCash)
                    .addComponent(txtCash, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMember1)
                    .addComponent(txtMember, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnReciept1))
                .addGap(12, 12, 12)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMemberStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbPromotion)
                    .addComponent(lblStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Point, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnMember, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnRegister, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnReciept))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPay)
                    .addComponent(btnDelete)
                    .addComponent(btnClear))
                .addGap(14, 14, 14))
        );

        jPanel1.setBackground(new java.awt.Color(48, 8, 40));
        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        tblRecieptDetail.setBackground(new java.awt.Color(48, 8, 40));
        tblRecieptDetail.setFont(new java.awt.Font("Rockwell Extra Bold", 0, 12)); // NOI18N
        tblRecieptDetail.setForeground(new java.awt.Color(255, 255, 255));
        tblRecieptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "name", "size", "qty", "price"
            }
        ));
        jScrollPane1.setViewportView(tblRecieptDetail);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void openCustomerDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        CustomerDialog customerDialog = new CustomerDialog(frame, editedCustomer);
        customerDialog.setLocationRelativeTo(this);
        customerDialog.setVisible(true);
        customerDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                System.out.println("Dialog Closed");
            }
        });
    }

    private void clearTextFieldAndCustomer() {
        lblPoint.setText("0");
        lblStatus.setText("Non-Member");
        reciept.setPayment_name("Cash");
        txtCash.setText("0");
        txtMember.setText("");
        reciept.setCustomerId(0);
        cmbPromotion.setSelectedIndex(0);
        cmbPromotion.setVisible(false);
        cmdPayment.setSelectedIndex(0);
        lblDiscount.setText("0.00");
        reciept.setDiscount(0);
        promotion = null;

    }


    private void cmdPaymentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdPaymentActionPerformed
        String paymentName = cmdPayment.getSelectedItem().toString();
        reciept.setPayment_name(paymentName);
        if (cmdPayment.getSelectedIndex() == 1) {
            txtCash.setText("0");
            txtCash.setEnabled(false); // Disable MoneyField
        } else {
            txtCash.setEnabled(true);  // Enable MoneyField
            reciept.setChange(0);
            reciept.setReceieved(overallTotal);
        }
    }//GEN-LAST:event_cmdPaymentActionPerformed

    private void btnRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterActionPerformed
        editedCustomer = new Customer();
        openCustomerDialog();
    }//GEN-LAST:event_btnRegisterActionPerformed

    private void txtCashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCashActionPerformed

    }//GEN-LAST:event_txtCashActionPerformed

    private void btnMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMemberActionPerformed
        openHistoryDialog();

    }//GEN-LAST:event_btnMemberActionPerformed

    private void btnRecieptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRecieptActionPerformed

        openReciept();

    }//GEN-LAST:event_btnRecieptActionPerformed

    private void openReciept() throws NumberFormatException {
        if (promotion != null) {
            String totalAsString = Float.toString(overallTotal - promotion.getDiscount()); // Convert float to string
            Totallbl.setText(totalAsString);

        } else {
            Totallbl.setText(Float.toString(overallTotal));

        }

        RecieptDia.setSize(355, 550); // Set the dialog to be 400 pixels wide and 300 pixels tall
        RecieptDia.setLocationRelativeTo(this);
        paymentName.setText(reciept.getPayment_name());
        lblEmpName.setText(userService.getCurrent().getName());
        Date obj = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String formattedDate = sdf.format(obj);
        recieptArea.setText("                                         Receipt                                        \n");
        recieptArea.append("                                        D-Coffee                                        \n");
        recieptArea.append("============================================\n");
        recieptArea.append(formattedDate + "\n");

        StringBuilder productDetails = new StringBuilder(); // To store product details (name, quantity, price, type, and size)

        for (RecieptDetail recieptDetail : reciept.getRecieptDetails()) {
            String product = recieptDetail.getProductName();
            int quantity = recieptDetail.getQty();
            float pricePerUnit = recieptDetail.getProductPrice();
            String type = recieptDetail.getProductType();
            String size = recieptDetail.getProducttSize();

            String productLine = product + " (Type: " + type + ", Size: " + size + ") x" + quantity + " THB " + pricePerUnit;
            productDetails.append(productLine).append("\n");
            // If you want to update the overallTotal, uncomment the line below.
            // overallTotal += recieptDetail.getTotalPrice();
        }

        // if qr code set received = total
        // and change = 0
        paymentName.setText(reciept.getPayment_name());
        String received = txtCash.getText();
//        lblVat.setText(date);
        if (Integer.parseInt(received) - reciept.getTotal() < 0) {
            lblRecieptChange.setText("0");
            float total = reciept.getTotal();
            String totalText = Float.toString(total - reciept.getDiscount());
            lblRecieptReceived.setText(totalText);
            System.out.println("1");
        } else {
            float change = (Integer.parseInt(received) - (reciept.getTotal() - reciept.getDiscount()));
            lblRecieptChange.setText(Float.toString(change));
            System.out.println("2");
            lblRecieptReceived.setText(received);

        }
        recieptArea.append(productDetails.toString()); // Append all product details
        RecieptDia.setVisible(true);
        recieptArea.append("============================================\n");
        recieptArea.append("                                        Thank You                                        \n");
    }

    private void btnFoodsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFoodsActionPerformed
        scrProductList.setViewportView(foodsListPanel);
        ArrayList<Product> products = (ArrayList<Product>) productService.getByCategoryId(2);
        foodsListPanel.setProducts(products);
        System.out.println(products);
    }//GEN-LAST:event_btnFoodsActionPerformed

    private void btnDrinksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDrinksActionPerformed
        scrProductList.setViewportView(productListPanel);
        ArrayList<Product> products = (ArrayList<Product>) productService.getByCategoryId(1);
        productListPanel.setProducts(products);
        System.out.println(products);
    }//GEN-LAST:event_btnDrinksActionPerformed

    private void btnBakeryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBakeryActionPerformed
        scrProductList.setViewportView(bakeryListPanel);
        ArrayList<Product> products = (ArrayList<Product>) productService.getByCategoryId(3);
        bakeryListPanel.setProducts(products);
        System.out.println(products);
    }//GEN-LAST:event_btnBakeryActionPerformed

    private void txtMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMemberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMemberActionPerformed

    private void btnReciept1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReciept1ActionPerformed
        String telFromField = txtMember.getText();
        System.out.println(telFromField);
        String customerName = customerService.findCustomerNameByTel(telFromField);
        editedCustomer = customerService.getCustomerById(customerId);

        if (customerName != null) {
            int cusIdInt = Integer.parseInt(customerService.findCustomerIdByTel(telFromField));
            lblStatus.setText(customerName);
            customerId = cusIdInt;
            lblRecieptMember.setText(customerName);
            cmbPromotion.setVisible(true);
            customer = customerService.getCustomerById(customerId);
            lblPoint.setText(String.valueOf(customer.getPoint()));
            reciept.setCustomerId(customerId);
            promotion = promotionService.getPromotion(1);
            lblDiscount.setText(String.valueOf(promotion.getDiscount()));

        } else {
            lblStatus.setText("Non Member");
        }


    }//GEN-LAST:event_btnReciept1ActionPerformed

    private void cmbPromotionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbPromotionActionPerformed
        //Index StartFrom 0
        int SelectedIndex;
        editedCustomer = customerService.getCustomerById(customerId);
        if (cmbPromotion.getSelectedIndex() == 0) {
            SelectedIndex = cmbPromotion.getSelectedIndex();
            System.out.println(SelectedIndex);
            promotion = promotionService.getPromotion(SelectedIndex + 1);
            lblDiscount.setText(String.valueOf(promotion.getDiscount()));
        } else if (cmbPromotion.getSelectedIndex() == 1) {
            SelectedIndex = cmbPromotion.getSelectedIndex();
            System.out.println(SelectedIndex);
            promotion = promotionService.getPromotion(SelectedIndex + 1);
            lblDiscount.setText(String.valueOf(promotion.getDiscount()));
            if (promotionService.isEnoughPoint(customer, promotion) == false) {
                cmbPromotion.setSelectedIndex(0);
                promotion = promotionService.getPromotion(1);
            }

        }
    }//GEN-LAST:event_cmbPromotionActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed

        int selrow = tblRecieptDetail.getSelectedRow();
        System.out.println(selrow);

        reciept.removeRecieptDetailAtIndex(selrow);
        refreshReciept();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPayActionPerformed
//        calculateChangeReceived();
        System.out.println("" + reciept);
        User currentUser = userService.getCurrent();
        reciept.setUserId(currentUser.getId());
        reciept.setReceieved(Integer.parseInt(txtCash.getText()));
        if (!recieptService.checkCashEnough(reciept, promotion) || !recieptService.checkEmptyCart(reciept)) {
            return;
        }
        if (promotion != null) {
            reciept.setDiscount(promotion.getDiscount());
        }
        if (!recieptService.checkCashEnough(reciept, promotion)) {
            System.out.println("Error");
            return; // Exit the function
        }
        if (customerId == 0) {
            recieptService.addNew(reciept, promotion = null);
            openReciept();
        } else {
            recieptService.addNew(reciept, promotion);
            openReciept();
            int cusId = reciept.getCustomerId();
            customerService.managePointAfterPurchase(customerService.getCustomerById(cusId), promotion);

        }


    }//GEN-LAST:event_btnPayActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        reciept.clearRecieptDetails();
        clearTextFieldAndCustomer();

        refreshReciept();
    }//GEN-LAST:event_btnClearActionPerformed

    private void RecieptDiaWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_RecieptDiaWindowClosing
        lblRecieptMember.setText("--");
        clearReciept();
        clearTextFieldAndCustomer();

    }//GEN-LAST:event_RecieptDiaWindowClosing

    private void RecieptDiaWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_RecieptDiaWindowActivated
        float vat = overallTotal * 0.07f;
        DecimalFormat decimalFormat = new DecimalFormat("#0.00"); // Format to 2 decimal places
        String formattedVAT = decimalFormat.format(vat);
        lblVat.setText(formattedVAT);
    }//GEN-LAST:event_RecieptDiaWindowActivated

//    private void selectDefaultPromotion() {
//        editedCustomer = customerService.getCustomerById(customerId);
//        int SelectedIndex;
//        int customerpoint;
//        SelectedIndex = cmbPromotion.getSelectedIndex();
//        System.out.println(SelectedIndex);
//        promotion = promotionService.getPromotion(SelectedIndex + 1);
//        System.out.println("RequiredPoint");
//        System.out.println(promotion.getRequiredPoints());
//        reciept.setDiscount(promotion.getDiscount());
//        System.out.println(customerId);
//        customerpoint = editedCustomer.getPoint();
//        CheckEnougnPointForPromotion(customerpoint);
//        lblDiscount.setText(String.valueOf(promotion.getDiscount()));
//    }
    private void openRecieptDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        RecieptDialog recieptDialog = new RecieptDialog(frame, true);
        recieptDialog.setLocationRelativeTo(this);
        recieptDialog.setVisible(true);
    }

    private void openHistoryDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        HistoryDialog historyDialog = new HistoryDialog(frame, true);
        historyDialog.setLocationRelativeTo(this);
        historyDialog.setVisible(true);
    }

//    private void openMemberDialog() {
//        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
//        MemberDialog memberDialog = new MemberDialog();
//        memberDialog.setLocationRelativeTo(this);
//        memberDialog.setVisible(true);
//    }
    private void clearReciept() {
        reciept = new Reciept();
        refreshReciept();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Discount;
    private javax.swing.JLabel Point;
    private javax.swing.JDialog RecieptDia;
    private javax.swing.JLabel Total;
    private javax.swing.JLabel Totallbl;
    private javax.swing.JLabel VAT;
    private javax.swing.JButton btnBakery;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnDrinks;
    private javax.swing.JButton btnFoods;
    private javax.swing.JButton btnMember;
    private javax.swing.JButton btnPay;
    private javax.swing.JButton btnReciept;
    private javax.swing.JButton btnReciept1;
    private javax.swing.JButton btnRegister;
    private javax.swing.JComboBox<String> cmbPromotion;
    private javax.swing.JComboBox<String> cmdPayment;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblCash;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblEmpName;
    private javax.swing.JLabel lblMember1;
    private javax.swing.JLabel lblMemberStatus;
    private javax.swing.JLabel lblPayment;
    private javax.swing.JLabel lblPoint;
    private javax.swing.JLabel lblRecieptChange;
    private javax.swing.JLabel lblRecieptMember;
    private javax.swing.JLabel lblRecieptReceived;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblVATonPos;
    private javax.swing.JLabel lblVat;
    private javax.swing.JLabel paymentName;
    private javax.swing.JTextArea recieptArea;
    private javax.swing.JScrollPane scrProductList;
    private javax.swing.JTable tblRecieptDetail;
    private javax.swing.JLabel total;
    private javax.swing.JTextField txtCash;
    private javax.swing.JLabel txtChange;
    private javax.swing.JTextField txtMember;
    private javax.swing.JLabel txtPayment;
    private javax.swing.JLabel txtReceived;
    private javax.swing.JLabel vat;
    // End of variables declaration//GEN-END:variables

    public void buy(Product product, String productSize, String productType, int qty) {
        boolean found = false;
        String type = "";
        String size = "";
        // Check if a product with the same name, size, and type already exists in the receipt
        reciept.updateRecieptDetails(product, productType, productSize, qty);
        refreshReciept();
    }

//    @Override
//    public void buy(Product product, String productSize, String productType, int qty) {
//        reciept.addRecieptDetail(product, productSize, productType, qty);
//        System.out.println(productType);
//
//        System.out.println(productSize);
//
//        refreshReciept();
//    }
}
