/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui;

import com.mycompany.dcoffeeproject.model.Reciept;
import com.mycompany.dcoffeeproject.model.RecieptDetail;
import com.mycompany.dcoffeeproject.service.CheckMaterialDetailService;
import com.mycompany.dcoffeeproject.service.RecieptService;
import java.util.Date;
import java.util.List;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author chanatip
 */
public class HistoryDialog extends javax.swing.JDialog {

    private List<Reciept> list;
    private List<RecieptDetail> details;
    private Reciept reciept;
    private final RecieptService RecieptService;
    private final RecieptDetail RecieptDetail;

    private int idRecieptDetail;

    /**
     * Creates new form HistoryDialog
     */
    public HistoryDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        RecieptService = new RecieptService();
        list = RecieptService.getReciepts();
        tblHistory.setModel(new RecieptTableModel(list));

        RecieptDetail = new RecieptDetail();

        //Edit this 
        //should be detail service
        details = RecieptService.getRecieptDetailByRecieptId(idRecieptDetail);
//            List<RecieptDetail> details = RecieptService.getRecieptsById(receiptId);

        tblDetail.setModel(new RecieptDetailTableModel(details));
        //tblDetail
    }

    public class RecieptDetailTableModel extends AbstractTableModel {

        private List<RecieptDetail> recieptDetails;
        private String[] columnNames = {"ID", "Product ID", "Product Name", "Product Price", "Qty", "Total Price", "Reciept ID", "Product Type", "Product Size"};

        public RecieptDetailTableModel(List<RecieptDetail> recieptDetails) {
            this.recieptDetails = recieptDetails;
        }

        @Override
        public int getRowCount() {
            return recieptDetails.size();
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    return recieptDetail.getId();
                case 1:
                    return recieptDetail.getProductId();
                case 2:
                    return recieptDetail.getProductName();
                case 3:
                    return recieptDetail.getProductPrice();
                case 4:
                    return recieptDetail.getQty();
                case 5:
                    return recieptDetail.getTotalPrice();
                case 6:
                    return recieptDetail.getRecieptId();
                case 7:
                    return recieptDetail.getProductType();
                case 8:
                    return recieptDetail.getProducttSize();
                default:
                    return "Unknown";
            }
        }
    }

    private static class RecieptTableModel implements TableModel {

        private List<Reciept> reciepts;
        private String[] columnNames = {"ID", "Date", "Total", "Received", "Change", "Payment", "QTY", "User ID", "Customer ID"};

        public RecieptTableModel(List<Reciept> reciept) {
            this.reciepts = reciept;
        }

        @Override
        public int getRowCount() {
            return reciepts.size();
        }

        @Override
        public int getColumnCount() {
            return 9;
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
//            throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            return false;
//            throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Reciept reciept = reciepts.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    return reciept.getId();
                case 1:
                    return reciept.getCreatedDate();
                case 2:
                    return reciept.getTotal();
                case 3:
                    return reciept.getChange();
                case 4:
                    return reciept.getReceieved();
                case 5:
                    return reciept.getPayment_name();
                case 6:
                    return reciept.getTotalQty();
                case 7:
                    return reciept.getUserId();
                case 8:
                    return reciept.getCustomerId();
                default:
                    return "Unknown";
            }
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            if (columnIndex == 0) {
                // Handle editing for the first column (assuming it's editable).
                // Update the data in your list, for example:
                reciepts.get(rowIndex).setId((Integer) aValue);
                // You should do similar updates for other columns as needed.

                // Notify listeners that the cell value has changed.
            }
            // You can add similar logic for other columns if they are editable.
        }

        @Override
        public void addTableModelListener(TableModelListener l) {
            //
        }

        @Override
        public void removeTableModelListener(TableModelListener l) {
            //
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            // Return the appropriate class for each column
            switch (columnIndex) {
                case 0:
                    return Integer.class; // Assuming ID is an Integer
                case 1:
                    return Date.class;    // Assuming Date is a Date type
                case 2:
                    return Double.class;  // Assuming Total is a Double
                // Add cases for other columns with their respective data types
                default:
                    return Object.class;  // Default to Object class
            }
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        DetailDialog = new javax.swing.JDialog();
        jPanel4 = new javax.swing.JPanel();
        scrListDetail = new javax.swing.JScrollPane();
        tblDetail = new javax.swing.JTable();
        btnBack = new javax.swing.JButton();
        btnClose = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        scrListReciept = new javax.swing.JScrollPane();
        tblHistory = new javax.swing.JTable();
        btnDetail = new javax.swing.JButton();
        btnCloseHistory = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        jPanel4.setBackground(new java.awt.Color(48, 8, 40));

        tblDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrListDetail.setViewportView(tblDetail);

        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnClose.setText("Close");
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Harrington", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(245, 109, 8));
        jLabel1.setText("Detail");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(scrListDetail, javax.swing.GroupLayout.DEFAULT_SIZE, 848, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnBack)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnClose))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrListDetail, javax.swing.GroupLayout.DEFAULT_SIZE, 391, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClose)
                    .addComponent(btnBack))
                .addContainerGap())
        );

        javax.swing.GroupLayout DetailDialogLayout = new javax.swing.GroupLayout(DetailDialog.getContentPane());
        DetailDialog.getContentPane().setLayout(DetailDialogLayout);
        DetailDialogLayout.setHorizontalGroup(
            DetailDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        DetailDialogLayout.setVerticalGroup(
            DetailDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel3.setBackground(new java.awt.Color(48, 8, 40));

        tblHistory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrListReciept.setViewportView(tblHistory);

        btnDetail.setText("View");
        btnDetail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDetailActionPerformed(evt);
            }
        });

        btnCloseHistory.setText("Close");
        btnCloseHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseHistoryActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Harrington", 1, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(245, 109, 8));
        jLabel3.setText("Sell History");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrListReciept, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 808, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnDetail)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCloseHistory))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrListReciept, javax.swing.GroupLayout.DEFAULT_SIZE, 437, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCloseHistory)
                    .addComponent(btnDetail))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCloseHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseHistoryActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCloseHistoryActionPerformed

    private void btnDetailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDetailActionPerformed
        int selectedRow = tblHistory.getSelectedRow();
        if (selectedRow >= 0) {
            Object idObject = tblHistory.getModel().getValueAt(selectedRow, 0);
            if (idObject instanceof Integer) {
                int id = (int) idObject;
                idRecieptDetail = id;
                System.out.println(id);

                // Update the details list based on the selected ID
                details = RecieptService.getRecieptDetailByRecieptId(idRecieptDetail);
                tblDetail.setModel(new RecieptDetailTableModel(details));
            } else {
                System.out.println("Invalid ID type.");
            }
        } else {
            System.out.println("No row selected.");
        }

        DetailDialog.setSize(860, 600);
        DetailDialog.setLocationRelativeTo(this);
        this.dispose();
        DetailDialog.setVisible(true);
    }//GEN-LAST:event_btnDetailActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        this.dispose();
        DetailDialog.setVisible(false);
        HistoryDialog dialog = new HistoryDialog(new javax.swing.JFrame(), true);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        this.dispose();
        DetailDialog.setVisible(false);
    }//GEN-LAST:event_btnCloseActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HistoryDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HistoryDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HistoryDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HistoryDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                HistoryDialog dialog = new HistoryDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDialog DetailDialog;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnCloseHistory;
    private javax.swing.JButton btnDetail;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane scrListDetail;
    private javax.swing.JScrollPane scrListReciept;
    private javax.swing.JTable tblDetail;
    private javax.swing.JTable tblHistory;
    // End of variables declaration//GEN-END:variables

}
