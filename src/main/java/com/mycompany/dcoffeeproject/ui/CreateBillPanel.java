/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui;

import com.mycompany.dcoffeeproject.model.Bill;
import com.mycompany.dcoffeeproject.model.BillDetail;
import com.mycompany.dcoffeeproject.model.Material;
import com.mycompany.dcoffeeproject.model.Vendor;
import com.mycompany.dcoffeeproject.service.BillService;
import com.mycompany.dcoffeeproject.service.MaterialService;
import com.mycompany.dcoffeeproject.service.UserService;
import com.mycompany.dcoffeeproject.service.VendorService;
import java.awt.Component;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;

/**
 *
 * @author USER
 */
public class CreateBillPanel extends javax.swing.JPanel {

    VendorService vendorService = new VendorService();
    private List<Vendor> vendors;
    MaterialService materialService = new MaterialService();
    private ArrayList<Material> materials;
    private BillService billService = new BillService();
    Bill bill;
    DefaultTableModel billDetailModel;
    private JComboBox<String> materialComboBox;
    private Material editedMaterial;
    private ArrayList<Material> selectedMaterialsFromOption;
    private BillPanel billPanel;

    /**
     * Creates new form CreateBillPanel
     */
    public CreateBillPanel() {
        initComponents();
        bill = new Bill();
        billService = new BillService();
        UserService userService = new UserService();
        bill.setUser(userService.getCurrent());
        pnlMaterial.setVisible(false);
        loadCbbVendor();
        loadCbbMaterial();
        cbbMaterial.setSelectedIndex(-1);
        cbbVendor.setSelectedIndex(-1);
        cbbMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                cbbMenuActionPerformed(evt);
            }
        });
        seletedCbbMateriablBillDetail();

        billDetailModel = new CustomDefaultTableModel(new Object[]{"Name", "Qty", "Unit", "Price Per Unit", "Total"}, 0);
        tblBillDetail.setModel(billDetailModel);
        tblBillDetail.getModel().addTableModelListener(e -> {
            if (e.getType() == TableModelEvent.UPDATE) {
                int row = e.getFirstRow();
                int col = e.getColumn();
                if (col >= 1 && col <= 3) { // ตรวจสอบเฉพาะ column qty, unit, และ pricePerUnit
                    updateMaterialAndBill(row, col);
                }
            }
        });

        tblBillDetail.setRowHeight(35);

    }

    void updateMaterialAndBill(int row, int col) {
        if (row >= 0 && row < bill.getBillDetails().size()) {
            BillDetail billDetail = bill.getBillDetails().get(row);
            int qty = Integer.parseInt(String.valueOf(tblBillDetail.getValueAt(row, 1)));
            String unit = String.valueOf(tblBillDetail.getValueAt(row, 2));
            float pricePerUnit = Float.parseFloat(String.valueOf(tblBillDetail.getValueAt(row, 3)));

            billDetail.setQty(qty);
            billDetail.setUnit(unit);
            billDetail.setPricePerUnit(pricePerUnit);
            billDetail.setTotal(qty * pricePerUnit);

            tblBillDetail.setValueAt(billDetail.getTotal(), row, 4);

            bill.calculateBillTotal();
            refreshBill();
        }
    }

    public void setSelectedMaterialsFromOption(ArrayList<Material> selectedMaterials) {
        this.selectedMaterialsFromOption = selectedMaterials;
    }

    private void refreshBill() {

        tblBillDetail.revalidate();
        tblBillDetail.repaint();
        lblTotal.setText("Total: " + bill.getTotalBill());

    }

    private void loadCbbVendor() {
        vendors = vendorService.getVendors();
        DefaultComboBoxModel<String> vendorModel = new DefaultComboBoxModel<>();
        for (Vendor v : vendors) {
            vendorModel.addElement(v.getName());
        }
        cbbVendor.setModel(vendorModel);
        cbbVendor.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (cbbVendor.getSelectedIndex() != -1) {
                    pnlMaterial.setVisible(true);

                    String selectedVendorName = cbbVendor.getSelectedItem().toString();
                    int selectedVendorId = -1;
                    for (Vendor vendor : vendors) {
                        if (vendor.getName().equals(selectedVendorName)) {
                            selectedVendorId = vendor.getId();
                            break;
                        }
                    }
                    if (selectedVendorId != -1) {

                        bill.setVendorId(selectedVendorId);
                    }

                    bill.setVendorId(selectedVendorId);
                } else {
                    pnlMaterial.setVisible(false);
                }
            }
        });
    }

    private void loadCbbMaterial() {
        materials = (ArrayList<Material>) materialService.getMaterials();
        DefaultComboBoxModel<String> materialModel = new DefaultComboBoxModel<>();
        for (Material m : materials) {
            materialModel.addElement(m.getName());
        }
        cbbMaterial.setModel(materialModel);
    }

    private void seletedCbbMateriablBillDetail() {
        cbbMaterial.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object selectedMaterial = cbbMaterial.getSelectedItem();

                if (selectedMaterial != null) {
                    String selectedMaterialName = selectedMaterial.toString();

                    boolean existingItem = false;
                    for (BillDetail billDetail : bill.getBillDetails()) {
                        if (billDetail.getName().equals(selectedMaterialName)) {
                            existingItem = true;
                            break;
                        }
                    }

                    if (!existingItem) {

                        Material material = materialService.getByName(selectedMaterialName);

                        if (material != null) {
                            bill.addBillDetail(material, material.getQty());

                            Object[] rowData = {material.getName(), 1, material.getUnit(), material.getPricePerUnit(), 0};
                            addRowToBillDetail(rowData);
                        }
                    }
                }
            }
        });
    }

    private void cbbMenuActionPerformed(ActionEvent evt) {
        Object selectedOption = cbbMenu.getSelectedItem();

        if (selectedOption != null) {
            String selectedOptionText = selectedOption.toString();

            if ("Select multiple material".equals(selectedOptionText)) {

                MaterialAddBillOption materialAddBillOption = new MaterialAddBillOption((JFrame) SwingUtilities.getRoot(this), true);
                materialAddBillOption.setLocationRelativeTo(null);
                materialAddBillOption.setVisible(true);

                ArrayList<Material> selectedMaterials = materialAddBillOption.getSelectedMaterials();
                for (Material material : selectedMaterials) {
                    addMaterialToBill(material);
                }
            }
        }
    }

    private void addMaterialToBill(Material material) {
        String selectedMaterialName = material.getName();

        boolean existingItem = false;
        for (BillDetail billDetail : bill.getBillDetails()) {
            if (billDetail.getName().equals(selectedMaterialName)) {
                existingItem = true;
                break;
            }
        }

        if (!existingItem) {
            bill.addBillDetail(material, material.getQty());
            Object[] rowData = {material.getName(), 1, material.getUnit(), material.getPricePerUnit(), 0};
            addRowToBillDetail(rowData);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jInternalFrame1 = new javax.swing.JInternalFrame();
        pnlVendor = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cbbVendor = new javax.swing.JComboBox<>();
        srcBillDetail = new javax.swing.JScrollPane();
        pnlMaterial = new javax.swing.JPanel();
        cbbMaterial = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        txtBuy = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtChange = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblBillDetail = new javax.swing.JTable();
        cbbMenu = new javax.swing.JComboBox<>();
        lblTotal = new javax.swing.JLabel();
        btnSaveBill = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();

        jInternalFrame1.setVisible(true);

        javax.swing.GroupLayout jInternalFrame1Layout = new javax.swing.GroupLayout(jInternalFrame1.getContentPane());
        jInternalFrame1.getContentPane().setLayout(jInternalFrame1Layout);
        jInternalFrame1Layout.setHorizontalGroup(
            jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jInternalFrame1Layout.setVerticalGroup(
            jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        setBackground(new java.awt.Color(255, 255, 255));

        pnlVendor.setBackground(new java.awt.Color(48, 8, 40));

        jLabel1.setForeground(new java.awt.Color(245, 109, 8));
        jLabel1.setText("Search Vendor:");

        cbbVendor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout pnlVendorLayout = new javax.swing.GroupLayout(pnlVendor);
        pnlVendor.setLayout(pnlVendorLayout);
        pnlVendorLayout.setHorizontalGroup(
            pnlVendorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlVendorLayout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbbVendor, javax.swing.GroupLayout.PREFERRED_SIZE, 355, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlVendorLayout.setVerticalGroup(
            pnlVendorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlVendorLayout.createSequentialGroup()
                .addContainerGap(21, Short.MAX_VALUE)
                .addGroup(pnlVendorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbbVendor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18))
        );

        srcBillDetail.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        pnlMaterial.setBackground(new java.awt.Color(235, 235, 235));

        cbbMaterial.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Buy:");

        txtBuy.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtBuy.setText("0");

        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("Change:");

        txtChange.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtChange.setText("0");

        tblBillDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblBillDetail);

        cbbMenu.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select material", "Select multiple material" }));

        lblTotal.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        lblTotal.setForeground(new java.awt.Color(0, 0, 0));
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTotal.setText("Total: 0");

        javax.swing.GroupLayout pnlMaterialLayout = new javax.swing.GroupLayout(pnlMaterial);
        pnlMaterial.setLayout(pnlMaterialLayout);
        pnlMaterialLayout.setHorizontalGroup(
            pnlMaterialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlMaterialLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(78, 78, 78))
            .addGroup(pnlMaterialLayout.createSequentialGroup()
                .addGap(160, 160, 160)
                .addComponent(cbbMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cbbMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 379, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtBuy, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtChange, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlMaterialLayout.createSequentialGroup()
                .addContainerGap(35, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1030, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );
        pnlMaterialLayout.setVerticalGroup(
            pnlMaterialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMaterialLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(pnlMaterialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbbMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtChange, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbbMenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 398, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        srcBillDetail.setViewportView(pnlMaterial);

        btnSaveBill.setText("Save");
        btnSaveBill.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveBillActionPerformed(evt);
            }
        });

        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlVendor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(srcBillDetail))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(btnSaveBill, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlVendor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(srcBillDetail)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSaveBill, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveBillActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveBillActionPerformed

        String buyText = txtBuy.getText();
        String changeText = txtChange.getText();

        if (!buyText.isEmpty() && !changeText.isEmpty()) {
            float buy = Float.parseFloat(buyText);
            float change = Float.parseFloat(changeText);
            bill.setBuy(buy);
            bill.setChange(change);
        }

        billService.addNew(bill);

        if (billPanel != null) {
            billPanel.refreshBillTable();
        }

        JFrame createBillFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
        createBillFrame.dispose();
        JOptionPane.showMessageDialog(this, "Create bill is successfullly.", "Create bill is successfullly.", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_btnSaveBillActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(this);
        frame.dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    public void addRowToBillDetail(Object[] data) {
        billDetailModel.addRow(data);
        updateMaterialAndBill(billDetailModel.getRowCount() - 1, 1); 
        refreshBill();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSaveBill;
    private javax.swing.JComboBox<String> cbbMaterial;
    private javax.swing.JComboBox<String> cbbMenu;
    private javax.swing.JComboBox<String> cbbVendor;
    private javax.swing.JInternalFrame jInternalFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JPanel pnlMaterial;
    private javax.swing.JPanel pnlVendor;
    private javax.swing.JScrollPane srcBillDetail;
    private javax.swing.JTable tblBillDetail;
    private javax.swing.JTextField txtBuy;
    private javax.swing.JTextField txtChange;
    // End of variables declaration//GEN-END:variables

    void setBillPanel(BillPanel aThis) {
        this.billPanel = aThis;
    }

}
