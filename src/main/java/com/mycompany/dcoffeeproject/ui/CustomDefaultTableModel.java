/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author USER
 */
public class CustomDefaultTableModel extends DefaultTableModel {

    public CustomDefaultTableModel(Object[] columnNames, int rowCount) {
        super(columnNames, rowCount);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        // อนุญาตให้แก้ไขข้อมูลในคอลัมน์ 1, 2, และ 3 (Qty, Unit, PricePerUnit)
        return column >= 1 && column <= 3;
    }
}