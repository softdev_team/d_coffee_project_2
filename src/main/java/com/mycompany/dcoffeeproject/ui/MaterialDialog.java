/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui;

import com.mycompany.dcoffeeproject.model.Bill;
import com.mycompany.dcoffeeproject.model.BillDetail;
import com.mycompany.dcoffeeproject.model.CheckMaterial;
import com.mycompany.dcoffeeproject.model.CheckMaterialDetail;
import com.mycompany.dcoffeeproject.model.Material;
import com.mycompany.dcoffeeproject.model.User;
import com.mycompany.dcoffeeproject.service.BillService;
import com.mycompany.dcoffeeproject.service.CheckMaterialDetailService;
import com.mycompany.dcoffeeproject.service.CheckMaterialService;
import com.mycompany.dcoffeeproject.service.MaterialService;
import com.mycompany.dcoffeeproject.service.UserService;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author User
 */
public class MaterialDialog extends javax.swing.JDialog {

    ArrayList<Material> materials;

    BillService billService = new BillService();
    CheckMaterialDetailService checkMaterialDetailService = new CheckMaterialDetailService();
    MaterialService materialService = new MaterialService();
    CheckMaterialService checkMaterialService = new CheckMaterialService();
    private CheckMaterial editedCheckMaterial;
    CheckMaterialDetail checkMaterialDetail = new CheckMaterialDetail();
    ArrayList<CheckMaterialDetail> checkMaterialDetailsList = new ArrayList<>();

    /**
     * Creates new form MaterialDialog
     */
    public MaterialDialog(java.awt.Frame parent, CheckMaterial editedCheckMaterial) {
        super(parent, true);
        initComponents();
        this.editedCheckMaterial = editedCheckMaterial;
        initMaterialTable();
        initCMDTable();

    }

    private void initCMDTable() {
        tblCheckMatDetail.getTableHeader().setFont(new Font("TH Sarabun New", Font.PLAIN, 20));
        tblCheckMatDetail.setRowHeight(20);
        editedCheckMaterial.setUser(UserService.getCurrent());
        tblCheckMatDetail.setModel(new AbstractTableModel() {
            String[] headers = {"ID", "Name", "Qty Last", "New Qty", "Qty Expire"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return editedCheckMaterial.getCheckMaterialDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<CheckMaterialDetail> checkMaterialDetails = editedCheckMaterial.getCheckMaterialDetails();
                CheckMaterialDetail cmd = checkMaterialDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return cmd.getMatId();
                    case 1:
                        return cmd.getName();
                    case 2:
                        return cmd.getQtyLast();
                    case 3:
                        return cmd.getRemain();
                    case 4:
                        return cmd.getQtyExpire();
                    default:
                        return "";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<CheckMaterialDetail> checkMD = editedCheckMaterial.getCheckMaterialDetails();
                CheckMaterialDetail cmd = checkMD.get(rowIndex);
                if (columnIndex == 3) { // Assuming New Qty is in column 3
                    int remainQty = Integer.parseInt((String) aValue);

                    if (remainQty < 1 || remainQty > cmd.getQtyLast()) {
                        return;
                    }

                    cmd.setRemain(remainQty);

                    refreshCMD();
                } else if (columnIndex == 4) { // Assuming Qty Expire is in column 4
                    int QtyExpire = Integer.parseInt((String) aValue);

                    cmd.setQtyExpire(QtyExpire);

                    refreshCMD();
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return switch (columnIndex) {
                    case 3, 4 ->
                        true;
                    default ->
                        false;
                };
                // Allow editing of New Qty and Qty Expire columns
            }
        });
    }

    private void initMaterialTable() {
        materials = materialService.getMaterialsOrderbyName1();
        tblMaterial.getTableHeader().setFont(new Font("TH Sarabun New", Font.PLAIN, 22));
        tblMaterial.setRowHeight(20);
        tblMaterial.setModel(new AbstractTableModel() {
            String[] hearders = {"ID", "Name", "Unit"};

            @Override
            public String getColumnName(int column) {
                return hearders[column];
            }

            @Override
            public int getRowCount() {
                return materials.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material material = materials.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return material.getId();
                    case 1:
                        return material.getName();
                    case 2:
                        return material.getUnit();
                    default:
                        return "";
                }

            }

        });
        tblMaterial.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = tblMaterial.rowAtPoint(e.getPoint());
                Material mat = materials.get(row);
                editedCheckMaterial.addCheckMaterialDetail(mat);
                System.out.println(editedCheckMaterial.getCheckMaterialDetails());
                refreshCMD();
            }
        });

    }

    private void refreshCMD() {
        tblCheckMatDetail.revalidate();
        tblCheckMatDetail.repaint();
//        lblTotal.setText("Total: " + checkMaterial .getTotalBill());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblMaterial = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblCheckMatDetail = new javax.swing.JTable();
        btnSave = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(48, 8, 40));

        jPanel1.setBackground(new java.awt.Color(48, 8, 40));

        tblMaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Mat Id", "Name", "Unit"
            }
        ));
        tblMaterial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblMaterialMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblMaterial);

        tblCheckMatDetail.setFont(new java.awt.Font("TH Sarabun New", 0, 24)); // NOI18N
        tblCheckMatDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Mat Id", "Name", "Quantity"
            }
        ));
        jScrollPane2.setViewportView(tblCheckMatDetail);

        btnSave.setFont(new java.awt.Font("TH Sarabun New", 0, 24)); // NOI18N
        btnSave.setText("save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 427, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 577, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(54, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        System.out.println("" + editedCheckMaterial.getCheckMaterialDetails());
        checkMaterialService.addNew(editedCheckMaterial);
        dispose();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void tblMaterialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblMaterialMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tblMaterialMouseClicked

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSave;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblCheckMatDetail;
    private javax.swing.JTable tblMaterial;
    // End of variables declaration//GEN-END:variables
}
