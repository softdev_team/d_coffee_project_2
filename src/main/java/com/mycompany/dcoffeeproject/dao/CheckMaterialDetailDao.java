/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;

import com.mycompany.dcoffeeproject.model.CheckMaterialDetail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USER
 */
public class CheckMaterialDetailDao implements Dao<CheckMaterialDetail> {

    @Override
    public CheckMaterialDetail get(int id) {
        
        CheckMaterialDetail checkMaterialDetail = null;
        String sql = "SELECT * FROM check_material_detail WHERE cmd_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkMaterialDetail = CheckMaterialDetail.fromRS(rs);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkMaterialDetail;
    }

    public List<CheckMaterialDetail> getAll() {
        ArrayList<CheckMaterialDetail> list = new ArrayList();
        String sql = "SELECT * check_material_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterialDetail checkMaterialDetail = CheckMaterialDetail.fromRS(rs);
                list.add(checkMaterialDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    
    
    @Override
    public List<CheckMaterialDetail> getAll(String where, String order) {
        ArrayList<CheckMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_material_detail where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterialDetail checkMaterialDetail = CheckMaterialDetail.fromRS(rs);
                list.add(checkMaterialDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckMaterialDetail> getAll(String order) {
        ArrayList<CheckMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_material_detail ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterialDetail checkMaterialDetail = CheckMaterialDetail.fromRS(rs);
                list.add(checkMaterialDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckMaterialDetail save(CheckMaterialDetail obj) {

        String sql = "INSERT INTO check_material_detail (cmd_name, cmd_qty_last, cmd_remain, cmd_qty_expire, mat_id, check_mat_id)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getQtyLast());
            stmt.setInt(3, obj.getRemain());
            stmt.setInt(4, obj.getQtyExpire());
            stmt.setInt(5, obj.getMatId());
            stmt.setInt(6, obj.getCheckMatId());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckMaterialDetail update(CheckMaterialDetail obj) {
        String sql = "UPDATE check_material_detail"
                + " SET cmd_name = ?, cmd_qty_last = ?, cmd_remain = ?, cmd_qty_expire = ?, mat_id = ?, check_mat_id = ? "
                + " WHERE cmd_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getQtyLast());
            stmt.setInt(3, obj.getRemain());
            stmt.setInt(4, obj.getQtyExpire());
            stmt.setInt(5, obj.getMatId());
            stmt.setInt(6, obj.getCheckMatId());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckMaterialDetail obj) {
        String sql = "DELETE FROM check_material_detail WHERE cmd_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    
    
    public int deleteByCheckMatId(int Id) {
        String sql = "DELETE FROM check_material_detail WHERE check_mat_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, Id);
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

//    public CheckMaterialDetail getAllCheckMaterialDetailById(int id) {
//        CheckMaterialDetail checkMaterialDetail = null;
//        String sql = "SELECT * FROM check_material_detail WHERE check_mat_id=?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, id);
//            ResultSet rs = stmt.executeQuery();
//
//            while (rs.next()) {
//                checkMaterialDetail = CheckMaterialDetail.fromRS(rs);
//            }
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return checkMaterialDetail;
//    }
    
    public List<CheckMaterialDetail> getAllCheckMaterialDetailByCheckId(int id) {
        ArrayList<CheckMaterialDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_material_detail WHERE check_mat_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CheckMaterialDetail checkMaterialDetail = CheckMaterialDetail.fromRS(rs);
                list.add(checkMaterialDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    

}
