/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.CheckMaterial;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kewwy
 */
public class CheckMaterialDao {

    public CheckMaterial get(int id) {
        CheckMaterial checkMaterial = null;
        String sql = "SELECT * FROM check_material WHERE check_material_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkMaterial = CheckMaterial.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkMaterial;
    }

    public List<CheckMaterial> getAll() {
        ArrayList<CheckMaterial> list = new ArrayList();
        String sql = "SELECT * FROM checkMaterial";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterial checkMaterial = CheckMaterial.fromRS(rs);
                list.add(checkMaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckMaterial> getAll(String where, String order) {
        ArrayList<CheckMaterial> list = new ArrayList();
        String sql = "SELECT * FROM check_material where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterial checkMaterial = CheckMaterial.fromRS(rs);
                list.add(checkMaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckMaterial> getAll(String order) {
        ArrayList<CheckMaterial> list = new ArrayList();
        String sql = "SELECT * FROM check_material  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterial checkMaterial = CheckMaterial.fromRS(rs);
                list.add(checkMaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public CheckMaterial save(CheckMaterial obj) {

        String sql = "INSERT INTO check_material (check_mat_date,check_mat_time,user_id)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String formattedDate = currentDate.format(dateFormatter);

        LocalTime currentTime = LocalTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        String formattedTime = currentTime.format(formatter);

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, formattedDate);
            stmt.setString(2, formattedTime);
            stmt.setInt(3, obj.getUserId());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setCheckMatId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public CheckMaterial update(CheckMaterial obj) {
        String sql = "UPDATE check_material"
                + " SET check_mat_date = ?, check_mat_time = ?, user_id = ?"
                + " WHERE check_mat_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String formattedDate = currentDate.format(dateFormatter);

        LocalTime currentTime = LocalTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        String formattedTime = currentTime.format(formatter);
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, formattedDate);
            stmt.setString(2, formattedTime);
            stmt.setInt(3, obj.getUserId());
            stmt.setInt(4, obj.getCheckMatId());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(CheckMaterial obj) {
        String sql = "DELETE FROM check_material WHERE check_mat_id=?";
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCheckMatId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public int deleteById(int id) {
        String sql = "DELETE FROM check_material WHERE check_mat_id=?";
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public int findLastId() {
        int lastId = -1;
        String sql = "SELECT check_mat_id FROM check_material ORDER BY check_mat_id DESC LIMIT 1";
        Connection conn = DatabaseHelper.getConnect();

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next()) {
                lastId = rs.getInt("check_mat_id");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return lastId;
    }

}
