/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;


import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.Reciept;
import com.mycompany.dcoffeeproject.model.RecieptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author chanatip
 */
public class RecieptDao implements Dao<Reciept> {
    @Override
    public Reciept get(int id) {
        Reciept reciept = null;
        String sql = "SELECT * FROM reciept WHERE reciept_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                reciept = Reciept.fromRS(rs);
                RecieptDetailDao rdd = new RecieptDetailDao();
                ArrayList<RecieptDetail> recieptDetails = (ArrayList<RecieptDetail>) rdd.getAll("reciept_id="+reciept.getId(),"reciept_detail_id");
                reciept.setRecieptDetails(recieptDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return reciept;
    }

  

    public List<Reciept> getAll() {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM reciept";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept reciept = Reciept.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Reciept> getAll(String where, String order) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM custoemr where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept reciept = Reciept.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Reciept> getAll(String order) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM reciept ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept reciept = Reciept.fromRS(rs);
                list.add(reciept);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Reciept save(Reciept obj) {

        String sql = "INSERT INTO reciept (reciept_queue, reciept_discount, reciept_total, reciept_received, reciept_change, payment_name, reciept_total_qty, user_id, customer_id)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getQueue());
            stmt.setFloat(2, obj.getDiscount());
            stmt.setFloat(3, obj.getTotal());
            stmt.setFloat(4, obj.getReceieved());
            stmt.setFloat(5, obj.getChange());
            stmt.setString(6, obj.getPayment_name());
            stmt.setInt(7, obj.getTotalQty());
            stmt.setInt(8, obj.getUserId());
            stmt.setInt(9, obj.getCustomerId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Reciept update(Reciept obj) {
        String sql = "UPDATE reciept"
                + " SET reciept_queue = ?, reciept_discount = ?, reciept_total = ?, reciept_received = ?, reciept_change = ?, payment_name = ?, reciept_total_qty = ?, user_id = ?, customer_id = ?"
                + " WHERE reciept_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getQueue());
            stmt.setFloat(2, obj.getDiscount());
            stmt.setFloat(3, obj.getTotal());
            stmt.setFloat(4, obj.getReceieved());
            stmt.setFloat(5, obj.getChange());
            stmt.setString(6, obj.getPayment_name());
            stmt.setInt(7, obj.getTotalQty());
            stmt.setInt(8, obj.getUserId());
            stmt.setInt(9, obj.getCustomerId());
            stmt.setInt(10, obj.getId());
            
            
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Reciept obj) {
        String sql = "DELETE FROM reciept WHERE reciept_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
    
    public List<RecieptDetail> getByRecieptId(int recieptId) {
        List<RecieptDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM reciept_detail WHERE reciept_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, recieptId);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                RecieptDetail recieptDetail = RecieptDetail.fromRS(rs);
                list.add(recieptDetail);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        System.out.println(list);
        return list;
    }
}
