/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.WaterBill;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mookda
 */
public class WaterBillDao implements Dao<WaterBill> {

    @Override
    public WaterBill get(int id) {
        WaterBill waterBill = null;
        String sql = "SELECT * FROM water_bill WHERE water_bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                waterBill = WaterBill.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return waterBill;
    }

    public List<WaterBill> getAll() {
        ArrayList<WaterBill> list = new ArrayList();
        String sql = "SELECT * FROM water_bill";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WaterBill waterBill = WaterBill.fromRS(rs);
                list.add(waterBill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<WaterBill> getAll(String where, String order) {
        ArrayList<WaterBill> list = new ArrayList();
        String sql = "SELECT * FROM water_bill where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WaterBill waterBill = WaterBill.fromRS(rs);
                list.add(waterBill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<WaterBill> getAll(String order) {
        ArrayList<WaterBill> list = new ArrayList();
        String sql = "SELECT * FROM water_bill  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WaterBill waterBill = WaterBill.fromRS(rs);
                list.add(waterBill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public WaterBill save(WaterBill obj) {

        String sql = "INSERT INTO water_bill (water_unit,  water_unit_price, water_total_price)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUnit());
            stmt.setFloat(2, obj.getUnitPrice());
            stmt.setFloat(3, obj.getTotalPrice());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public WaterBill update(WaterBill obj) {
        String sql = "UPDATE water_bill"
                + " SET water_unit = ?, water_unit_price = ?, water_total_price = ?"
                + " WHERE water_bill_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUnit());
            stmt.setFloat(2, obj.getUnitPrice());
            stmt.setFloat(3, obj.getTotalPrice());
            stmt.setInt(4, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(WaterBill obj) {
        String sql = "DELETE FROM water_bill WHERE water_bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
