/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.ElectricBill;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mookda
 */
public class ElectricBillDao implements Dao<ElectricBill> {

    @Override
    public ElectricBill get(int id) {
        ElectricBill electricBill = null;
        String sql = "SELECT * FROM electric_bill WHERE electric_bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                electricBill = ElectricBill.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return electricBill;
    }

    public List<ElectricBill> getAll() {
        ArrayList<ElectricBill> list = new ArrayList();
        String sql = "SELECT * FROM electric_bill";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ElectricBill electricBill = ElectricBill.fromRS(rs);
                list.add(electricBill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<ElectricBill> getAll(String where, String order) {
        ArrayList<ElectricBill> list = new ArrayList();
        String sql = "SELECT * FROM electric_bill where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ElectricBill electricBill = ElectricBill.fromRS(rs);
                list.add(electricBill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ElectricBill> getAll(String order) {
        ArrayList<ElectricBill> list = new ArrayList();
        String sql = "SELECT * FROM electric_bill  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ElectricBill electricBill = ElectricBill.fromRS(rs);
                list.add(electricBill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ElectricBill save(ElectricBill obj) {

        String sql = "INSERT INTO electric_bill (electric_unit,  electric_unit_price, electric_total_price)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUnit());
            stmt.setFloat(2, obj.getUnitPrice());
            stmt.setFloat(3, obj.getTotalPrice());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public ElectricBill update(ElectricBill obj) {
        String sql = "UPDATE electric_bill"
                + " SET electric_unit = ?, electric_unit_price = ?, electric_total_price = ?"
                + " WHERE electric_bill_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUnit());
            stmt.setFloat(2, obj.getUnitPrice());
            stmt.setFloat(3, obj.getTotalPrice());
            stmt.setInt(4, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ElectricBill obj) {
        String sql = "DELETE FROM electric_bill WHERE electric_bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
