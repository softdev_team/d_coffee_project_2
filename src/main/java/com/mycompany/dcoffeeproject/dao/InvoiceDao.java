/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.Invoice;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mookda
 */
public class InvoiceDao implements Dao<Invoice> {

    @Override
    public Invoice get(int id) {
        Invoice invoice = null;
        String sql = "SELECT * FROM invoice WHERE invoice_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                invoice = Invoice.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return invoice;
    }

    public List<Invoice> getAll() {
        ArrayList<Invoice> list = new ArrayList();
        String sql = "SELECT * FROM invoice";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Invoice invoice = Invoice.fromRS(rs);
                list.add(invoice);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Invoice> getAll(String where, String order) {
        ArrayList<Invoice> list = new ArrayList();
        String sql = "SELECT * FROM invoice where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Invoice invoice = Invoice.fromRS(rs);
                list.add(invoice);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Invoice> getAll(String order) {
        ArrayList<Invoice> list = new ArrayList();
        String sql = "SELECT * FROM invoice  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Invoice invoice = Invoice.fromRS(rs);
                list.add(invoice);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Invoice save(Invoice obj) {

        String sql = "INSERT INTO invoice (invoice_total, rental_bill_id, water_bill_id, electric_bill_id)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setInt(2, obj.getRentalId());
            stmt.setInt(3, obj.getWaterId());
            stmt.setInt(4, obj.getElectricId());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Invoice update(Invoice obj) {
        String sql = "UPDATE invoice"
                + " SET invoice_total = ?, rental_bill_id = ?, water_bill_id = ?, electric_bill_id = ?"
                + " WHERE invoice_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setInt(2, obj.getRentalId());
            stmt.setInt(3, obj.getWaterId());
            stmt.setInt(4, obj.getElectricId());
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Invoice obj) {
        String sql = "DELETE FROM invoice WHERE invoice_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
