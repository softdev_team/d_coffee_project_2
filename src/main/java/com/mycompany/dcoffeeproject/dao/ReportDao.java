/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.PosReport;
import com.mycompany.dcoffeeproject.model.PosReport;
import com.mycompany.dcoffeeproject.model.Report;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kanthapong
 */
public class ReportDao implements Dao<Report> {

    @Override
    public Report get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Report> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Report save(Report obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Report update(Report obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Report obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Report> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<PosReport> getProductByTotalQuantity(int limit) {
        ArrayList<PosReport> list = new ArrayList();
        String sql = """
                     SELECT product_id as ID ,product_name as Name, SUM(qty) as Qty FROM reciept_detail
                     GROUP BY product_name
                     ORDER BY Qty DESC
                     LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                PosReport obj = PosReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<PosReport> getProductByTotalQuantity(String begin, String end, int limit) {
        ArrayList<PosReport> list = new ArrayList();
        String sql = """
                     SELECT product_name as Name, SUM(qty) as Qty FROM reciept_detail
                     INNER JOIN reciept rec ON rec.reciept_id = reciept_detail.reciept_id 
                            AND rec.reciept_date BETWEEN ? AND ?
                     GROUP BY product_name
                     ORDER BY Qty DESC 
                     LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                PosReport obj = PosReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    //น้อย
    public List<PosReport> getProductByTotalQuantityMin(int limit) {
        ArrayList<PosReport> list = new ArrayList();
        String sql = """
                     SELECT product_id as ID ,product_name as Name, SUM(qty) as Qty FROM reciept_detail
                     GROUP BY product_name
                     ORDER BY Qty ASC
                     LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                PosReport obj = PosReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    //matmin
    public List<PosReport> getMatByTotalQuantityMin(int limit) {
        ArrayList<PosReport> list = new ArrayList();
        String sql = """
                     SELECT mat_id as ID,mat_name as Name,mat_quantity as Qty 
                     FROM material
                     ORDER BY Qty ASC
                     LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                PosReport obj = PosReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    //ยอดขาย
     public List<PosReport> getMonthByTotalPrice(int limit) {
        ArrayList<PosReport> list = new ArrayList();
        String sql = """
                     SELECT strftime('%m', invoice_date) as Month,
                            invoice_total as Expenses,
                            SUM(reciept_total) as Income,
                            invoice_total - SUM(reciept_total) as Total
                     FROM invoice
                     INNER JOIN reciept ON invoice.invoice_id = reciept.invoice_id
                     GROUP BY Month
                     LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                PosReport oop = PosReport.fromRSS(rs);
                list.add(oop);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
     
     //ยอดขาย
     public List<PosReport> getMonthByTotalPrice(String begin, String end, int limit) {
        ArrayList<PosReport> list = new ArrayList();
        String sql = """
                     SELECT strftime('%m', invoice_date) as Month,
                            invoice_total as Expenses,
                            SUM(reciept_total) as Income,
                            invoice_total - SUM(reciept_total) as Total
                     FROM invoice
                     INNER JOIN reciept ON invoice.invoice_id = reciept.invoice_id
                     GROUP BY Month
                     LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                PosReport oop = PosReport.fromRSS(rs);
                list.add(oop);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
