/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.Bill;
import com.mycompany.dcoffeeproject.model.BillDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USER
 */
public class BillDao implements Dao<Bill> {
    
    @Override
    public Bill get(int id) {
        Bill bill = null;
        String sql = "SELECT * FROM bill WHERE bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                bill = Bill.fromRS(rs);
                BillDetailDao bdd = new BillDetailDao();
                ArrayList<BillDetail> billDetail = (ArrayList<BillDetail>) bdd.getAll("bill_id="+bill.getId(), " bill_detail_id");
                bill.setBillDetails(billDetail);   
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return bill;
    }
    
    public List<Bill> getAll() {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);
                
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Bill> getAll(String where, String order) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill where " + where + "  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);
                
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Bill> getAll(String order) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);
                
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public Bill save(Bill obj) {
        
        String sql = "INSERT INTO bill (bill_total, bill_buy, bill_change, user_id, vendor_id)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotalBill());
            stmt.setFloat(2, obj.getBuy());
            stmt.setFloat(3, obj.getChange());
            stmt.setInt(4, obj.getUserId());
            stmt.setInt(5, obj.getVendorId());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }
    
    @Override
    public Bill update(Bill obj) {
        String sql = "UPDATE bill"
                + " SET bill_total = ?, bill_buy = ?, bill_change = ?, user_id = ?, vendor_id = ?"
                + " WHERE bill_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotalBill());
            stmt.setFloat(2, obj.getBuy());
            stmt.setFloat(3, obj.getChange());
            stmt.setInt(4, obj.getUserId());
            stmt.setInt(5, obj.getVendorId());
            stmt.setInt(6, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    @Override
    public int delete(Bill obj) {
        String sql = "DELETE FROM bill WHERE bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    
}
