/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.mycompany.dcoffeeproject.dao.BillDao;
import com.mycompany.dcoffeeproject.dao.BillDetailDao;
import com.mycompany.dcoffeeproject.dao.MaterialDao;
import com.mycompany.dcoffeeproject.model.Bill;
import com.mycompany.dcoffeeproject.model.BillDetail;
import com.mycompany.dcoffeeproject.model.Material;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USER
 */
public class TestBillDetailDao {

    public static void main(String[] args) {
        BillDetailDao bdd = new BillDetailDao();
        for (BillDetail bd : bdd.getAll()) {
            System.out.println(bd);
        }
        BillDao bd = new BillDao();
        MaterialDao md = new MaterialDao();
        List<Material> materials = md.getAll();
        Material material0 = materials.get(2);
        Bill bill = bd.get(1);
        BillDetail newBillDetail = new BillDetail(material0.getId(), material0.getName(), 10, "pack", 5, 50, bill.getId());
        bdd.save(newBillDetail);
    }
}
