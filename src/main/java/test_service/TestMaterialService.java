/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.mycompany.dcoffeeproject.model.Material;
import com.mycompany.dcoffeeproject.service.MaterialService;

/**
 *
 * @author USER
 */
public class TestMaterialService {

    public static void main(String[] args) {
        MaterialService pd = new MaterialService();
        for (Material material : pd.getMaterials()) {
            System.out.println(material);
        }
    }

}
