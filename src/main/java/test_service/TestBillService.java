/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.mycompany.dcoffeeproject.dao.BillDao;
import com.mycompany.dcoffeeproject.dao.MaterialDao;
import com.mycompany.dcoffeeproject.model.Bill;
import com.mycompany.dcoffeeproject.model.BillDetail;
import com.mycompany.dcoffeeproject.model.Material;
import com.mycompany.dcoffeeproject.service.BillService;
import java.util.List;

/**
 *
 * @author USER
 */
public class TestBillService {

    public static void main(String[] args) {
        BillService billService = new BillService();
        System.out.println("All Bills:");
        for (Bill bill : billService.getBills()) {
            System.out.println(bill);
        }

        // create new bill
        Bill b1 = new Bill(500, 20, 1, 1);

        //load mat
        MaterialDao md = new MaterialDao();
        List<Material> materials = md.getAll();

        Material material0 = materials.get(1);

        //create bill
        float newMatQty = material0.getQty() * material0.getPricePerUnit();
//        BillDetail newBillDetail = new BillDetail(-1, "New Mat", 100, "unit", 10.0f, 100.0f, -1);
        BillDetail newBillDetail = new BillDetail(material0.getId(), material0.getName(),100, material0.getUnit(), material0.getPricePerUnit(),material0.getQty()*material0.getPricePerUnit(), -1);

        // save bill detail into bill
        b1.addBillDetail(newBillDetail);

//        System.out.println("Bill Before Adding:");
//        System.out.println(b1);
        //add bill into db
        Bill addedBill = billService.addNew(b1);
        System.out.println("Added Bill:");
        System.out.println(addedBill);

        // show after update 
        System.out.println("Materials After Update:");
        for (Material material : md.getAll()) {
            System.out.println(material);
        }
    }
}
